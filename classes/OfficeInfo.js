//OfficeInfo
//Child Class of LocationInfo

import { Component } from "react";
import PropTypes from 'prop-types';

//Classes
import LocationInfo from './LocationInfo'

class OfficeInfo extends Component {

    static propTypes = {
        ...LocationInfo.propTypes,
    };

    constructor(props) {
        super(props);

        this.isOffice = true;
    }
  }

export default OfficeInfo;