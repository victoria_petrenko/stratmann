//ContainerInfo
//Child Class of LocationInfo

//Modules
import { Component } from "react";

//Classes
import LocationInfo from './LocationInfo'

class ContainerInfo extends Component {

    static propTypes = {
        ...LocationInfo.propTypes,
    }

    constructor(props) {
        super(props);
    }
  }

export default ContainerInfo;