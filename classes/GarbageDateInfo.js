//GarbageDateInfo
import { Component } from "react";

class GarbageDateInfo extends Component {

    constructor(props) {
        super(props);

        this.date = this.date;
        this.dateText = this.dateText;
        this.garbageTypeText = this.garbageTypeText;
        this.garbageType = this.garbageType;
        this.garbageDescription = this.garbageDescription;
        this.garbageDetailDescription = this.garbageDetailDescription;
        this.descriptionTitle = this.descriptionTitle;
    }
  }

export default GarbageDateInfo;