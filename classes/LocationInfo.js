//LocationInfo

//Modules
import { Component } from "react";
import PropTypes from 'prop-types';

class LocationInfo extends Component {

    static propTypes = {
        latitude: PropTypes.number,
        longitude: PropTypes.number,
        cityIndex: PropTypes.number,
        startWorkHours: PropTypes.number,
        endWorkHours: PropTypes.number,
        title: PropTypes.string,
        address: PropTypes.string,
        officeName: PropTypes.string,
        country: PropTypes.string,
        id: PropTypes.number,
        isOffice: PropTypes.bool
    }

    constructor(props) {
        super(props);

        this.latitude = this.props.latitude;
        this.longitude = this.props.longitude;
        this.title = this.props.title;
        this.address = this.props.address;
    }
  }

export default LocationInfo;