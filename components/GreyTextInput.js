//GreyTextInput.js

import React, { Component } from "react";
import {
    StyleSheet,
    TextInput,
} from "react-native";

export class GreyTextInput extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TextInput
                  placeholder={this.props.placeholder}
                  multiline={true}
                  scrollEnabled={true}
                  blurOnSubmit={true}
                  returnKeyType='done'
                  placeholderTextColor="#9B9B9B"
                  style={this.props.useBorder == true ? styles.containerWithBorder : styles.container}
                  onChangeText={this.props.onChangeText}
                  keyboardType= {this.props.keyboardType ? this.props.keyboardType : 'default'}
                >
            </TextInput>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: "helvetica",
        fontSize: 15,
        backgroundColor: '#EFEFF4',
        color: 'black',
        marginLeft: 16,
        marginRight: 16,
        marginTop: 10,
        height: 130,
        paddingLeft: 8,
        paddingTop: 14,
        marginBottom: 10,
        textAlignVertical: "top"
      },
      containerWithBorder: {
        fontFamily: "helvetica",
        fontSize: 15,
        borderColor: '#979797',
        borderWidth: 1,
        backgroundColor: '#EFEFF4',
        color: 'black',
        marginLeft: 16,
        marginRight: 16,
        marginTop: 10,
        height: 130,
        paddingLeft: 8,
        paddingTop: 14,
        marginBottom: 10,
        textAlignVertical: "top"
      },
});

export default GreyTextInput;