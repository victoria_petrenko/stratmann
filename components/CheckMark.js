//CheckMark.js

import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    TouchableWithoutFeedback
} from "react-native";

//Helpers
import { images } from '../Helpers/Image+Helper';

export class CheckMark extends Component {

    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
    }

    toggle() {
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => this.props.onPressCheckedButton()}>

                <View style={styles.container}>

                    <View style={styles.viewContainer}>
                        <View style={{ alignSelf: 'center', }}></View>
                        <ImageBackground style={{ marginRight: 10, alignSelf: 'center', width: 20, height: 20, alignItems: 'center', }}
                            resizeMode="contain" source={images.checkMarkSquareIcon.uri}>
                            <Image style={{ top: 4, }}
                                resizeMode="contain" source={this.props.checked ? images.checkedBoxIcon.uri : 'clear'} />
                        </ImageBackground>
                    </View>

                    <View style={styles.viewContainer}>
                        <Text style={styles.labelContainer}>
                            {this.props.label}
                        </Text>
                    </View>

                </View>

            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 20,
        marginRight: 20,
        margin: 10,
        flexDirection: "row",
    },
    viewContainer: {
        flexDirection: "row",
    },
    markContainer: {
        flexDirection: "column",
    },
    labelContainer: {
        color: "#423875",
        fontFamily: "helvetica",
        fontSize: 17,
        textAlign: "left"
    },
});

export default CheckMark;
