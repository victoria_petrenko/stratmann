//GradientButton.js

import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
} from "react-native";

//Modules
import LinearGradient from "react-native-linear-gradient";
import { View } from "react-native-animatable";

//Helpers


export class GradientButton extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={this.props.onPress}
            >
                <LinearGradient
                    colors={["#5A36D9", "rgba(90, 54, 217, 0.7)"]}
                    style={styles.buttonContainer}
                    start={{x: 0, y: 0}} end={{x: 0.5, y: 0}}
                >
                    <View style={styles.textView}>
                        <Text style={styles.textContainer}>
                            {this.props.title}
                        </Text>
                    </View>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 70,
        marginRight: 70,
        flexDirection: "column",
        margin: 10,
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        borderRadius: 22,
    },
    textView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textContainer: {
        color: "#fff",
        fontFamily: "helvetica",
        fontSize: 13,
        textAlign: "center",
        alignItems: 'center',
    },
});

export default GradientButton;