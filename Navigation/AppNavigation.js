//AppNavigation

//Modules
import React from "react";
import {
  Animated,
  Easing,
  Image,
  YellowBox,
  View,
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  DrawerItems
} from "react-navigation";

//Screens
import { ForgottenPasswordScreen } from "../screens/ForgottenPasswordScreen/ForgottenPasswordScreen";
import { LoginScreen } from "../screens/LoginScreen/LoginScreen";
import { SignupScreen } from "../screens/SignupScreen/SignupScreen";
import { HomeScreen } from "../screens/HomeScreen/HomeScreen";
import { GarbageRemovalScreen } from "../screens/GarbageRemovalScreen/GarbageRemovalScreen";
import { LocationsScreen } from "../screens/LocationsScreen/LocationsScreen";
import { WasteScreen } from "../screens/WasteScreen/WasteScreen";
import { ContainerServicesScreen } from "../screens/ContainerServicesScreen/ContainerServicesScreen";
import { ContactsScreen } from "../screens/ContactsScreen/ContactsScreen";
import { AboutUsScreen } from "../screens/AboutUsScreen/AboutUsScreen";
import { NewsScreen } from "../screens/NewsScreen/NewsScreen";
import { ToolsScreen } from "../screens/ToolsScreen/ToolsScreen";
import {GarbageTimeLineScreen} from "../screens/GarbageTimeLineScreen/GarbageTimeLineScreen";
import {LocationsListScreen} from '../screens/LocationsListScreen/LocationsListScreen';
import {LocationDetailScreen} from '../screens/LocationDetailScreen/LocationDetailScreen';

//Helpers
import { images } from "../Helpers/Image+Helper";
import { strings } from "../Helpers/Localization+Helper";
import { fromLeft } from 'react-navigation-transitions';

const DrawerContent = props => (
  <View>
    <View
      style={{
        height: 115,
        alignItems: "center",
        justifyContent: "center"
      }}
    >
    <Image source={images.menuLogoIcon.uri}></Image>
    </View>
    <DrawerItems {...props} />
  </View>
);

const DrawerStack = createDrawerNavigator(
  {
    HomeScreen: { screen: HomeScreen },
    GarbageRemovalScreen: { screen: GarbageRemovalScreen },
    LocationsScreen: { screen: LocationsScreen },
    WasteScreen: { screen: WasteScreen },
    ContainerServicesScreen: { screen: ContainerServicesScreen },
    ContactsScreen: { screen: ContactsScreen },
    AboutUsScreen: { screen: AboutUsScreen },
    NewsScreen: { screen: NewsScreen },
    ToolsScreen: { screen: ToolsScreen },
  },
  {
    headerMode: "none",
    contentComponent: DrawerContent, 
  }
);

const DrawerNavigation = createStackNavigator(
  { 
    DrawerStack: { screen: DrawerStack},
    HomeScreen: { screen: HomeScreen },
    GarbageRemovalScreen: { screen: GarbageRemovalScreen },
    LocationsScreen: { screen: LocationsScreen },
    WasteScreen: { screen: WasteScreen },
    ContainerServicesScreen: { screen: ContainerServicesScreen },
    ContactsScreen: { screen: ContactsScreen },
    AboutUsScreen: { screen: AboutUsScreen },
    NewsScreen: { screen: NewsScreen },
    ToolsScreen: { screen: ToolsScreen },
    GarbageTimeLineScreen: {screen : GarbageTimeLineScreen},
    LocationsListScreen: {screen: LocationsListScreen},
    LocationDetailScreen: {screen: LocationDetailScreen}
  },
  {
    headerMode: "none",
  }
);

// login stack
const LoginStack = createStackNavigator(
  {
    loginScreen: { screen: LoginScreen },
    signupScreen: { screen: SignupScreen },
    forgottenPasswordScreen: {
      screen: ForgottenPasswordScreen,
      navigationOptions: { title: "Forgot Password" }
    }
  },
  {
    headerMode: "float",
    navigationOptions: {
      headerStyle: { backgroundColor: "red" },
      title: strings.notLoggedInTitle
    }
  }
);

const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
});

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    loginStack: { screen: DrawerNavigation },
    // drawerStack: { screen: DrawerNavigation }, 
  },
  {
    // Default config for all screens
    headerMode: "none",
    title: "Main",
    initialRouteName: "loginStack",
    transitionConfig: noTransitionConfig,
  }
);

export default PrimaryNav;
