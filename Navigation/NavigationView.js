//NavigationView
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  TouchableOpacity
} from "react-native";

export class NavigationView extends Component {
  static useBackgroundColor = false;
  render() {
    return (
      <View
        style={[
          styles.container,
          {
            backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : "#6753D5"
          }
        ]}
      >
        <TouchableOpacity
          style={styles.headerLeftContainer}
          onPress={() => this.props.onPressLeftButton()}
        >
          <Image resizeMode="contain" source={this.props.leftButton} />
        </TouchableOpacity>
        <View style={{ flex: 1 }}>
          <Text style={styles.titleContainer}>{this.props.titleLabel}</Text>
        </View>
        <TouchableOpacity
          style={styles.headerRightContainer}
          onPress={() => this.props.onPressAdditionalRightButton()}
        >
          <Image
            resizeMode="contain"
            source={this.props.additionalRightButton}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.headerRightContainer}
          onPress={() => this.props.onPressRightButton()}
        >
          <Image resizeMode="contain" source={this.props.rightButton} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    alignItems: "center",
    flexDirection: "row",
    height: 44,
    width: "100%",
    marginTop: Platform.OS == "ios" ? 20 : 0
  },
  headerLeftContainer: {
    alignItems: "center",
    marginRight: 16,
    marginLeft: 16
  },
  titleContainer: {
    color: "#fff",
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: "left"
  },
  headerRightContainer: {
    alignItems: "center",
    marginRight: 16
  }
});

export default NavigationView;
