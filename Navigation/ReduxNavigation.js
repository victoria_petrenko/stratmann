import React from 'react'
import ReactNavigation from 'react-navigation'
import { connect } from 'react-redux'
import AppNavigation from './AppNavigation'
import {createReduxBoundAddListener} from 'react-navigation-redux-helpers';

function ReduxNavigation (props) {
  const addListener = createReduxBoundAddListener("root");
  const { dispatch, nav } = props
  const navigation = ReactNavigation.navigation={
    dispatch: dispatch,
    state: nav,
    addListener,
 }
  return <AppNavigation navigation={navigation} />
}

const mapStateToProps = state => ({ nav: state.nav })
export default connect(mapStateToProps)(ReduxNavigation)
