//DrawerLabel
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native'

//Helpers
import renderIf from '../Helpers/renderIf';

export class DrawerLabel extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={this.props.smallRow == true ? styles.smallRowСontainer : styles.rowСontainer}>
                    <View style={styles.imageIconContainer}>
                        <Image resizeMode="contain" source={this.props.icon} />
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.labelContainer}>{this.props.label}</Text>
                    </View>
                </View>
                {renderIf(this.props.useSeparator)(
                    <View style={styles.separatorContainer}>
                    </View>
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        // height: 40,
        width: '100%',
        marginTop: 10,
    },
    rowСontainer: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 40,
        paddingLeft: 10,
        width: '100%'
    },
    smallRowСontainer: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 30,
        paddingLeft: 10,
        width: '100%'
    },
    imageIconContainer: {
        alignItems: 'center',
        marginRight: 24,
        width: 40
    },
    labelContainer: {
        color: '#423875',
        fontFamily: "helvetica",
        fontSize: 15,
        textAlign: 'left'
    },
    separatorContainer: {
        flexDirection: 'column',
        backgroundColor: '#D8D8D8',
        height: 1,
        justifyContent: 'center',
        margin: 15,
        marginBottom: 5
    }
});

export default DrawerLabel;