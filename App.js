//App.js

//Modules
import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { Provider } from 'react-redux'
import ReduxNavigation from './Navigation/ReduxNavigation'
import createStore from './Helpers/Store'
import { EventRegister } from 'react-native-event-listeners'
import "babel-polyfill"

//Services
import DBService from './services/DBService'
import LocationsService from './services/LocationsService'

console.ignoredYellowBox = ['Remote debugger'];

const store = createStore();

//Modules

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.locations = DBService.sharedInstance().geLocationsData();
    this.locationService = LocationsService.sharedInstance();
    this.state = {
      locations: this.locations,
    };
  }

  //Life-Cycle
  async componentWillMount() {

    this.listener = EventRegister.addEventListener('changedDBLocationList', (newLocations) => {
      this.setState({
        locations: newLocations,
      });
    })
  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <ReduxNavigation>
          </ReduxNavigation>
        </View>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
})
