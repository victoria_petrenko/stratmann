//ItemService.js

import { db } from '../DB/db.js';

export const addLocationItem =  (item) => {
    db.ref('/en/locationList').push({
        latitude: item.latitude,
        longitude: item.longitude,
        name: "",
        address: "",
        cityIndex: item.cityIndex,
        city: "",
    });
}

export const addOfficeItem =  (item) => {
    db.ref('/en/OfficeList').push({
        latitude: item.latitude,
        longitude: item.longitude,
        name: "",
        address: "",
        cityIndex: item.cityIndex,
        city: "",
    });
}

export const addContainerItem =  (item) => {
    db.ref('/en/ContainerList').push({
        latitude: item.latitude,
        longitude: item.longitude,
        name: "",
        address: "",
        cityIndex: item.cityIndex,
        city: "",
    });
}