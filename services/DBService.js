//DBService.js

//Modules
import { EventRegister } from 'react-native-event-listeners'

import { addLocationItem, addOfficeItem, addContainerItem } from './ItemService';
import { db } from '../DB/db.js';

//Services
import LocationsService from './LocationsService'

const GARBAGE_LOCATIONS_PAGE_SIZE = 20;

export default class DBService {

    static shared = null;

    static sharedInstance() {
        if (DBService.shared == null) {
            DBService.shared = new DBService();
        }

        return DBService.shared;
    }


    _locations = [];
    _currentGarbageLocationsPage = 0;

    /**
     * @returns {DBService}
     */

    constructor() {

        this.locations = this.locations === 'undefined' ? this.getLocations() : this.locations,

            this.state = {
                locations: this.locations,
            }
    }

    //Getters
    geLocationsData() {
        return this._locations.length > 0 ? this._locations : this.getLocations();
    }

    getCurrentGarbageLocationsPage() {
        return this._currentGarbageLocationsPage;
    }

    getLocationsDataPageSize() {
        var pageSize = this.geLocationsData().length/GARBAGE_LOCATIONS_PAGE_SIZE;
        return Math.round(pageSize);
    }

    getLocationsDataForPage(page) {

        var locations = this._locations.length > 0 ? this._locations : [];
        var limitedLocations = [];
        var loadPageLength = (page == 0 ? 1 : page) * GARBAGE_LOCATIONS_PAGE_SIZE;
        var currentPageLength = page * GARBAGE_LOCATIONS_PAGE_SIZE;
        if (locations.length > loadPageLength) {
            limitedLocations = locations.slice(currentPageLength, GARBAGE_LOCATIONS_PAGE_SIZE + currentPageLength);
        }
        
        if (this.needUpdateLocationsDataForPage(page)) {
            this.setCurrentGarbageLocationsPage(page);
            EventRegister.emit('loadedNewLocationsDataPage', page);
        }
        return limitedLocations;
    }

    //Setters
    setLocationsData(data) {
        this._locations = data;
    }

    setCurrentGarbageLocationsPage(page) {
        this._currentGarbageLocationsPage = page;
    }

    needUpdateLocationsDataForPage(page) {

        let currentPage = this.getCurrentGarbageLocationsPage();
        return page != currentPage;
    }

    addContainerItem(item) {

        db.ref('/en/ContainerList').push({
            latitude: item.latitude,
            longitude: item.longitude,
            name: "",
            address: "",
            cityIndex: item.cityIndex,
            city: "",
        });
    }

    addOfficeItem(item) {

        db.ref('/en/OfficeList').push({
            latitude: item.latitude,
            longitude: item.longitude,
            name: "",
            address: "",
            cityIndex: item.cityIndex,
            city: "",
        });
    }

    addLocationItem = (item) => {
        db.ref('/en/locationList').push({
            latitude: item.latitude,
            longitude: item.longitude,
            officeName: item.officeName,
            address: item.address ? item.address : "",
            cityIndex: item.cityIndex,
            country: item.country ? item.country : "",
            startWorkHours: item.startWorkHours,
            endWorkHours: item.endWorkHours,
            garbageType: item, garbageType
        });
    }

    addisOfficeLocation = (item) => {

        var isOffice = this.getRandomisOfficeValue();
        let locationPath = "/en/locationList/" + item.id;
        return db.ref(locationPath).update({
            isOffice: isOffice
        })
    }

    getRandomisOfficeValue() {
        return Math.random() >= 0.5;
    }

    getLocations() {

        var keys = [];
        var ref = db.ref().child('/en/locationList');
        ref.once('value', function (snap) {

            snap.forEach(function (item) {
                var itemVal = item.val();
                keys.push(itemVal);
            });

            if (keys.length == 0) {
                keys = DBService.sharedInstance().createLocationListDB();
            }

            var currentLocations = DBService.sharedInstance().geLocationsData();
            if (keys && keys.length != currentLocations.length) {
                DBService.sharedInstance().setLocationsData(keys);
                EventRegister.emit('changedDBLocationList', keys);
            }
        });
        return keys;
    }


    createLocationListDB() {

        var containerMarkers = LocationsService.sharedInstance().locationMarkers;
        for (index in containerMarkers) {
            var object = containerMarkers[index];
            this.addLocationItem(object)
        }
        return containerMarkers;
    }

    //Rendering
    render() {
        return null;
    }
}
