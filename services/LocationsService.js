//LocationsService

//Modules
import { Component } from "react";
import PropTypes from 'prop-types';
import { EventRegister } from 'react-native-event-listeners'

//Classes
import OfficeInfo from '../classes/OfficeInfo'
import ContainerInfo from '../classes/ContainerInfo'
import LocationInfo from '../classes/LocationInfo'

//Helpers

const offices = ['Arrow Sanitary',
    'City Sanitary Service',
    'Gresham Sanitary Service',
    'Heiberg Garbage',
    'Portland Disposal & Recycling',
    'Recology',
    'Republic Services Portland',
    'Rockwood Solid Waste',
    'Sunset Garbage',
    'Trashco Services',
    'Wacker Sanitary',
    'Walker Garbage',
    'Waste Management',
    'Elmers Sanitary',
    'Gruetter Sanitary Service',
    'Lehl Disposal',
    'Republic Services Lake Oswego',
    'Chetco Construction',
    'City of Roses Drop Box',
    'Flannerys Drop Box',
    'Interstate Drop Box',
    'KB Recycling',
    'Multnomah/Washington County Drop Box',
    'Papasadero & Sons',
    'Resource Recovery',
    'ReNu Recycling',
    'River City Environmental',
    'S & H Recycling',
    'Sea-Tac Disposal',
    'Eastside Disposal',
    'Kent-Meridian Disposal',
    'Rabanco Connections',
    'Recology CleanScapes',
    'American Disposal',
    'Waste Management – Northwest',
    'Waste Management – Rainier',
    'Waste Management – Sno-King',
    'Waste Management – South Sound',
    'Algona Transfer Station',
    'Houghton Recycling & Transfer Station',
    '​Waste Pro (unincorp. Lee) & City of Fort Myers',
    '​Waste Pro',
    'City of Cape Coral (Waste Pro)',
    '​Advanced Disposal (unincorp. Lee) & City of Fort Myers',
    '​Waste Pro',
    'Advanced Disposal',
    'City of Cape Coral (​​Waste Pro)',
    '​​Waste Pro',
    '​​Waste Pro (unincorp. Lee) & City of Fort Myers',
    'City of Cape Coral  (Waste Pro)',
    '​City of Fort Myers',
    'Advanced Disposal (unincorp. Lee) & City of Fort Myers',
    '​Waste Management',
    'Advanced Disposal'];

export default class LocationsService {

    static shared = null;

    static sharedInstance() {
        if (LocationsService.shared == null) {
            LocationsService.shared = new LocationsService();
            this.updateCurrentPosition();
        }

        return LocationsService.shared;
    }

    _renderedMapLocations = [];
    _watchID = null;
    _currentLatitude = 0;
    _currentLongitude = 0;
    _locationError = null;

    constructor() {

        this.containerMarkers = null,
            this.officeMarkers = null,
            this.locationMarkers = null,
            this.renderedMapLocations = this.getRenderedMapLocations();
            this.locationError = null;

        this.state = {
            containerMarkers: this.containerMarkers,
            officeMarkers: this.officeMarkers,
            locationMarkers: this.locationMarkers,
            currentLatitude: this.getCurrentLatitude(),
            currentLongitude: this.getCurrentLongitude(),
            locationError: this.locationError,
        }
    }

    updateCurrentPosition() {

        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setCurrentLatitude(position.coords.latitude);
                this.setCurrentLongitude(position.coords.longitude);
                EventRegister.emit('changedCurrentLocation', position);
            },
            (error) => this.setLocationError(error.message),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },

        );
    }

    addWatchPosition() {

        this._watchID = navigator.geolocation.watchPosition((lastPosition) => {
            this.setCurrentLatitude(lastPosition.coords.latitude);
            this.setCurrentLongitude(lastPosition.coords.longitude);
            EventRegister.emit('changedCurrentLocation', lastPosition);
        });
    }

    removeWatchPosition() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    //Getters
    getCurrentLatitude() {
        return this._currentLatitude;
    }

    getCurrentLongitude() {
        return this._currentLongitude;
    }

    //Setters
    setCurrentLatitude(lat) {
        this._currentLatitude = lat;
    }

    setCurrentLongitude(lng) {
        this._currentLongitude = lng;
    }

    setLocationError(error) {
        this._locationError = error;
    }
    onRegionChange = (region) => {
        console.log('onRegionChange', region);
    };

    //Methods
    getContainerMarkers() {

        var markers = [];

        for (index = 0; index < 60; index++) {

            var office = new ContainerInfo();
            office.latitude = this.getRandomLatitude(64, 51);
            office.longitude = this.getRandomLongitude(-103, -122);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        for (index = 0; index < 20; index++) {

            var office = new ContainerInfo();
            office.latitude = this.getRandomLatitude(5, -38);
            office.longitude = this.getRandomLongitude(-64, -59);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        for (index = 0; index < 20; index++) {

            var office = new ContainerInfo();
            office.latitude = this.getRandomLatitude(31, -21);
            office.longitude = this.getRandomLongitude(-12, 39);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        for (index = 0; index < 100; index++) {

            var office = new ContainerInfo();
            office.latitude = this.getRandomLatitude(74, 21);
            office.longitude = this.getRandomLongitude(5, 126);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        return markers;
    }

    getOfficeMarkers() {

        var markers = [];

        for (index = 0; index < 60; index++) {

            var office = new OfficeInfo();
            office.latitude = this.getRandomLatitude(67, 27);
            office.longitude = this.getRandomLongitude(-106, -120);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        for (index = 0; index < 40; index++) {

            var office = new OfficeInfo();
            office.latitude = this.getRandomLatitude(6, -35);
            office.longitude = this.getRandomLongitude(-65, -58);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        for (index = 0; index < 50; index++) {

            var office = new OfficeInfo();
            office.latitude = this.getRandomLatitude(33, -22);
            office.longitude = this.getRandomLongitude(-11, 43);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        for (index = 0; index < 150; index++) {

            var office = new OfficeInfo();
            office.latitude = this.getRandomLatitude(75, 21);
            office.longitude = this.getRandomLongitude(5, 130);;
            office.title = 'Niederlassung Paderborn';
            office.address = 'Halberstadter Str. 14';
            office.cityIndex = this.getRandomCityIndex();
            office.startWorkHours = this.getRandomStartWorkHours();
            office.endWorkHours = this.getRandomEndWorkHours();
            office.officeName = this.getRandomOfficeName();
            markers.push(office);
        }

        return markers;
    }

    getLocationMarkers() {

        var locationMarkers = [];
        var containerMarkers = this.containerMarkers;
        var officeMarkers = this.officeMarkers;

        containerMarkers.map(location => (
            locationMarkers.push(location)
        ));

        officeMarkers.map(location => (
            locationMarkers.push(location)
        ));

        return locationMarkers;
    }

    getRandomCityIndex() {
        return this.getRandomIntInclusive(1000, 10000);
    }

    getRandomStartWorkHours() {
        return this.getRandomIntInclusive(8, 11);
    }

    getRandomEndWorkHours() {
        return this.getRandomIntInclusive(17, 20);
    }

    getRandomOfficeName() {
        return offices[Math.floor(Math.random() * offices.length)];
    }

    getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    getRandomLatitude(from, to) {
        return this.getRandomLocation(from, to);
    }

    getRandomLongitude(from, to) {
        return this.getRandomLocation(from, to);
    }

    getRandomLocation(from, to) {
        return (Math.random() * (to - from) + from).toFixed(4) * 1;
    }

    getRenderedMapLocations() {
        return this._renderedMapLocations;
    }

    setRenderedMapLocation(location) {
        return this._renderedMapLocations.push(location);
    }

    isRenderedMapLocation(location) {
        if (this._renderedMapLocations.length > 0) {
            for (var i = 0; i < this._renderedMapLocations.length; i++) {
                if (this._renderedMapLocations[i] == location)
                    return true;
            }
        }
        return false;
    }

    //Rendering
    render() {
        return null;
    }
}
