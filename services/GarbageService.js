//GarbageService

//Components
import { Component } from "react";
import XDate from "xdate";
import GarbageDateInfo from "../classes/GarbageDateInfo";

//Helpers
import { strings } from "../Helpers/Localization+Helper";

var GarbageTypeEnum = {
  YellowDumpsterType: 0,
  BlueDumpsterType: 1,
  GreenDumpsterType: 2,
  BrownDumpsterType: 3,
  properties: {
    0: {
      type: "Yellow dumpster",
      description: "Metal, plastic products",
      detailDescription: ["Synthetic envelope", "Synthetic bags", "Synthetic packages", "Synthetic wrappers", "Plastic bottles from washing-up liquids", "Plastic bottles from detergents and cosmetics", "Plastic bottles for drinks", "Plastic boxes and cups", "Styrofoam", "Tetra Pak", "Cardboard packaging for drinks and milk", "Vacuum packing", "Cans from cans and drinks", "Locks", "Bowls and covers made of aluminum"]
    },
    1: { type: "Blue dumpster", description: "Paper waste", detailDescription: ["Paper"] },
    2: { type: "Green dumpster", description: "Glass and articles thereof", detailDescription: ["Beverages from drinks", "Glass cans", "Glass jars for jam", "Pharmaceutical glass containers", "Other glass containers"] },
    3: { type: "Brown dumpster", description: "Food and other biowaste", detailDescription: ["Remnants of vegetables and fruits", "Paper filters for coffee makers and tea bags", "Broken meat", "Mowed grass, leaves", "Cut flowers", "Cut branches", "Houseplants", "Hygienic filler-litter for small animals (straw, hay or sawdust)", "Small animals littering"] }
  },
  count: 4,
};


export default class GarbageService {

  static shared = null;

  static infoData = null;
  static selectedInfoData = null;

  static sharedInstance() {
    if (GarbageService.shared == null) {
      GarbageService.shared = new GarbageService();
    }

    return GarbageService.shared;
  }

  constructor() {

    this.state = {
      garbageDates: this.garbageDates(),
      garbageInfoData: this.infoData,
      selectedGarbageInfoData: this.selectedInfoData,
      garbageTypeDetailsData: this.getGarbageTypeDetailDescription(),
    }
  }

  //Methods
  garbageDates = () => {

    if (this.garbageDates.length > 0) {
      return this.garbageDates;
    }

    var dates = new Array();

    var randomDate = require('random-datetime');
    var currentDate = XDate()
    for (month = 1; month < 13; month++) {

      for (index = 0; index < 10; index++) {

        var randomDay = randomDate({
          year: currentDate.getFullYear(),
          month: month
        });

        dates.push(randomDay);
      }
    }

    dates.push(currentDate);

    if (!this.infoData) {
      this.infoData = this.garbageInfoData(dates);
    }

    return dates;
  };

  garbageInfoData = (dates) => {

    let datesInfoArray = [];
    if (dates) {
      for (index in dates) {

        var date = dates[index];
        var dateText = this.dateText(date);

        var garbageInfo = new GarbageDateInfo();
        garbageInfo.date = date;
        garbageInfo.dateText = dateText;

        garbageInfo.garbageType = this.getGarbageType(Math.floor(Math.random() * GarbageTypeEnum.count));
        garbageInfo.garbageTypeText = this.getGarbageTypeText(garbageInfo.garbageType);
        garbageInfo.garbageDescription = this.garbageDescription(garbageInfo.garbageType);
        garbageInfo.garbageDetailDescription = this.garbageDetailDescription(garbageInfo.garbageType);
        datesInfoArray.push(garbageInfo)
      }
    }
    return datesInfoArray;
  }

  getTime = (date) => {

    // Creating variables to hold time.
    var TimeType, hour, minutes, fullTime;

    // Getting current hour from Date object.
    hour = date.getHours();

    // Checking if the Hour is less than equals to 11 then Set the Time format as AM.
    if (hour <= 11) {

      TimeType = 'am';

    }
    else {
      // If the Hour is Not less than equals to 11 then Set the Time format as PM.
      TimeType = 'pm';

    }

    // IF current hour is grater than 12 then minus 12 from current hour to make it in 12 Hours Format.
    if (hour > 12) {
      hour = hour - 12;
    }

    // If hour value is 0 then by default set its value to 12, because 24 means 0 in 24 hours time format. 
    if (hour == 0) {
      hour = 12;
    }


    // Getting the current minutes from date object.
    minutes = date.getMinutes();

    // Checking if the minutes value is less then 10 then add 0 before minutes.
    if (minutes < 10) {
      minutes = '0' + minutes.toString();
    }

    // Adding all the variables in fullTime variable.
    fullTime = hour.toString() + ':' + minutes.toString() + ' ' + TimeType.toString();
    return fullTime;
  }

  dateText = (date) => {
    return ('0' + date.getDate()).slice(-2) + '. ' + ('0' + (date.getMonth() + 1)).slice(-2) + '. ' + date.getFullYear() + ' (' + this.getTime(date) + ')';
  }


  selectedInfoDataForDate = (selectedDate) => {

    var selectedDates = []
    var selectedInfoData = this.state.garbageDates.length > 0 ? this.state.garbageDates : this.garbageDates();

    selectedInfoData = selectedInfoData.filter(function (item) {

      var selectedYear, selectedMonth, selectedDay;

      if (selectedDate.year && selectedDate.month && selectedDate.day) {
        selectedYear = selectedDate.year;
        selectedMonth = selectedDate.month;
        selectedDay = selectedDate.day;
      } else {
        selectedYear = selectedDate.getFullYear();
        selectedMonth = selectedDate.getMonth() + 1;
        selectedDay = selectedDate.getDate();
      }

      if (item.getFullYear() == selectedYear && item.getMonth() + 1 == selectedMonth && item.getDate() == selectedDay) {
        selectedDates.push(item);
      }
    }).map(function ({ id, name, city }) {
      return null;
    });

    this.selectedInfoData = this.garbageInfoData(selectedDates);
    return this.selectedInfoData;
  }

  selectedDayForDetailGarbage = (selectedGarbage) => {

    var currentDate = XDate();
    var selectedDate = currentDate;

    this.state.garbageInfoData.filter(function (item) {
      if (selectedGarbage.garbageType == item.garbageType && currentDate <= item.date) {
        if (selectedDate == currentDate || item.date < selectedDate) {
          selectedDate = item.date;
        } 
      }
    })
    return selectedDate;
  }

  getGarbageType = (garbageNumber) => {

    var type = GarbageTypeEnum.YellowDumpsterType;
    switch (garbageNumber) {
      case 1: type = GarbageTypeEnum.BlueDumpsterType; break;
      case 2: type = GarbageTypeEnum.GreenDumpsterType; break;
      case 3: type = GarbageTypeEnum.BrownDumpsterType; break;
    }
    return type;
  }

  getGarbageTypeText = (garbageType) => {

    var garbageTypeText = GarbageTypeEnum.properties[0].type;
    garbageTypeText = GarbageTypeEnum.properties[garbageType].type;
    return garbageTypeText;
  }

  getGarbageTypeDetailDescription() {

    var detailDescriptions = [];
    for (index = 0; index < GarbageTypeEnum.count; index++) {

      let detailDesciptionTypes = this.garbageDetailDescription(index);;

      for (type = 0; type < detailDesciptionTypes.length; type++) {
        var garbageInfo = new GarbageDateInfo();
        garbageInfo.garbageType = index;
        garbageInfo.garbageTypeText = this.getGarbageTypeText(garbageInfo.garbageType);
        garbageInfo.garbageDescription = this.garbageDescription(garbageInfo.garbageType);
        garbageInfo.garbageDetailDescription = this.garbageDetailDescription(garbageInfo.garbageType);
        garbageInfo.descriptionTitle = detailDesciptionTypes[type];
        detailDescriptions.push(garbageInfo)
      }
    }
    return detailDescriptions;
  }

  garbageDescription = (garbageType) => {
    var garbageDescription = GarbageTypeEnum.properties[garbageType].description;
    return garbageDescription;
  }

  garbageDetailDescription = (garbageType) => {

    let garbageDetailDescription = GarbageTypeEnum.properties[garbageType].detailDescription;
    return garbageDetailDescription;
  }

  render() {
    return null;
  }
}