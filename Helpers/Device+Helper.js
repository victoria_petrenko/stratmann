//Device+Helper.js

//Modules
import React, { Component } from 'react';
import {
    Dimensions,
    Platform,
} from 'react-native';
import DeviceInfo from "react-native-device-info";

export default class DeviceHelper {

    static shared = null;

    static sharedInstance() {
        if (DeviceHelper.shared == null) {
            DeviceHelper.shared = new DeviceHelper();
        }

        return DeviceHelper.shared;
    }

    constructor() {

        this.state = {

        }
    }

    //Methods
    isPortrait = () => {
        return Dimensions.get('window').height > Dimensions.get('window').width;
    };

    isiOS = () => {
        return Platform.OS === 'ios';
    }

    getDeviceLocale() {

        var deviceLocale = DeviceInfo.getDeviceLocale();

        if (deviceLocale.includes("de")) {
            deviceLocale = "de";
        } else if (deviceLocale.includes("en")) {
            deviceLocale = "en";
        } else if (deviceLocale.includes("ru")) {
            deviceLocale = "ru";
        } else {
            deviceLocale = "en";
        }
        return deviceLocale;
    }

    getVersionNumber() {
        return DeviceInfo.getVersion()
    }

    getBuildVersion() {
        return DeviceInfo.getBuildVersion()
    }

    getBundleIdentifier() {
        return DeviceInfo.getBundleIdentifier()
    }
    
    //Rendering
    render() {
        return null;
    }
}