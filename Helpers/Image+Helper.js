//Image+Helper

const images = {
    menuIcon: {
      imgName: 'Menu', 
      uri: require('../assets/navigationIcons/menu-icon/menu-icon.png')
    },
    bagIcon: {
      imgName: 'Bag', 
      uri: require('../assets/navigationIcons/bag-icon/bag-icon.png')
    },
    logoIcon: {
      imgName: 'Logo', 
      uri: require('../assets/homeScreenIcons/logo-icon/logo-icon.png')
    }, 
    timeIcon: {
      imgName: 'Time', 
      uri: require('../assets/homeScreenIcons/time-icon/time-icon.png')
    },
    calendarIcon: {
      imgName: 'Calendar', 
      uri: require('../assets/homeScreenIcons/calendar-icon/calendar-icon.png')
    },
    locationIcon: {
      imgName: 'Location', 
      uri: require('../assets/homeScreenIcons/location-icon/location-icon.png')
    },
    wasteIcon: {
      imgName: 'Waste', 
      uri: require('../assets/homeScreenIcons/waste-icon/waste-icon.png')
    }, 
    containerIcon: {
      imgName: 'Container', 
      uri: require('../assets/homeScreenIcons/container-icon/container-icon.png')
    },
    menuLogoIcon: {
      imgName: 'MenuLogo', 
      uri: require('../assets/navigationIcons/menu-logo-icon/menu-logo.png')
    }, 
    menuHomeIcon: {
      imgName: 'MenuHome', 
      uri: require('../assets/navigationIcons/menu-home-icon/menu-home-icon.png')
    },
    menuCalendarIcon: {
      imgName: 'MenuCalendar', 
      uri: require('../assets/navigationIcons/menu-calendar-icon/menu-calendar-icon.png')
    },
    menuLocationsIcon: {
      imgName: 'MenuLocations', 
      uri: require('../assets/navigationIcons/menu-locations-icon/menu-locations-icon.png')
    },
    menuWasteIcon: {
      imgName: 'MenuWaste', 
      uri: require('../assets/navigationIcons/menu-waste-icon/menu-waste-icon.png')
    },
    menuContainerIcon: {
      imgName: 'MenuContainer', 
      uri: require('../assets/navigationIcons/menu-container-icon/menu-container-icon.png')
    },
    menuContainerIcon: {
      imgName: 'MenuContainer', 
      uri: require('../assets/navigationIcons/menu-container-icon/menu-container-icon.png')
    },
    menuContactIcon: {
      imgName: 'MenuContact', 
      uri: require('../assets/navigationIcons/menu-contact-icon/menu-contact-icon.png')
    },
    menuAboutIcon: {
      imgName: 'MenuAbout', 
      uri: require('../assets/navigationIcons/menu-about-icon/menu-about-icon.png')
    },
    menuNewsIcon: {
      imgName: 'MenuNews', 
      uri: require('../assets/navigationIcons/menu-news-icon/menu-news-icon.png')
    },
    menuToolsIcon: {
      imgName: 'MenuTools', 
      uri: require('../assets/navigationIcons/menu-tools-icon/menu-tools-icon.png')
    },
    settingsIcon: {
      imgName: 'Settings', 
      uri: require('../assets/navigationIcons/settings-icon/settings-icon.png')
    },
    timelineIcon: {
      imgName: 'Timeline', 
      uri: require('../assets/garbageRemovalScreenIcons/timeline-icon/timeline-icon.png')
    },
    leftArrowIcon: {
      imgName: 'LeftArrow', 
      uri: require('../assets/garbageRemovalScreenIcons/left-arrow-icon/left-arrow-icon.png')
    },
    rightArrowIcon: {
      imgName: 'LeftArrow', 
      uri: require('../assets/garbageRemovalScreenIcons/right-arrow-icon/right-arrow-icon.png')
    },
    menuBackIcon: {
      imgName: 'MenuBackIcon', 
      uri: require('../assets/navigationIcons/menu-back-icon/menu-back-icon.png')
    }, 
    filterIcon: {
      imgName: 'FilterIcon', 
      uri: require('../assets/navigationIcons/filter-icon/filter-icon.png')
    }, 
    calloutBGIcon: {
      imgName: 'CalloutBGIcon', 
      uri: require('../assets/locationsScreenIcons/callout-bg-icon/callout-bg-icon.png')
    }, 
    detailMapIcon: {
      imgName: 'DetailMapIcon', 
      uri: require('../assets/locationsScreenIcons/detail-icon/detail-icon.png')
    },
    navigateMapIcon: {
      imgName: 'NavigateMapIcon', 
      uri: require('../assets/locationsScreenIcons/navigation-map-icon/navigation-map-icon.png')
    },
    moreMapIcon: {
      imgName: 'MoreMapIcon', 
      uri: require('../assets/locationsScreenIcons/more-map-icon/more-map-icon.png')
    },
    checkedBoxIcon: {
      imgName: 'CheckedBoxIcon', 
      uri: require('../assets/locationsScreenIcons/checked-box-icon/checked-box-icon.png')
    },
    checkMarkSquareIcon: {
      imgName: 'CheckMarkSquareIcon',
      uri: require('../assets/locationsScreenIcons/check-mark-square-icon/check-mark-square-icon.png')
    },
    smallContainerImage: {
      imgName: 'SmallContainerImage', 
      uri: require('../assets/images/containerServicesScreen/small-container.png')
    },
    middleContainerImage: {
      imgName: 'MiddleContainerImage', 
      uri: require('../assets/images/containerServicesScreen/middle-container.png')
    },
    middleMetallContainerImage: {
      imgName: 'MiddleMetallContainerImage', 
      uri: require('../assets/images/containerServicesScreen/middle-metall-container.png')
    },
    smallPaperContainerImage: {
      imgName: 'SmallPaperContainerImage', 
      uri: require('../assets/images/containerServicesScreen/small-paper-container.png')
    },
    bigContainerImage: {
      imgName: 'bigContainerImage', 
      uri: require('../assets/images/containerServicesScreen/big-container.png')
    },
    closeIcon: {
      imgName: 'CloseImage',
      uri: require('../assets/navigationIcons/close-icon/close-icon.png')
    },
    addIcon: {
      imgName: 'AddImage',
      uri: require('../assets/containerServicesScreenIcons/add-icon/add-icon.png')
    },
    otherProfileIcon: {
      imgName: 'OtherProfileIcon', 
      uri: require('../assets/aboutUsScreenIcons/other-profile-icon/other-profile-icon.png')
    },
    dataProtectionIcon: {
      imgName: 'DataProtectionIcon',
      uri: require('../assets/aboutUsScreenIcons/data-protection-icon/data-protection-icon.png'),
    },
    impressumIcon: {
      imgName: 'ImpressumIcon', 
      uri: require('../assets/aboutUsScreenIcons/impressum-icon/impressum-icon.png')
    }
  }
  
export {images};
