//Store
import { combineReducers, createStore, applyMiddleware} from 'redux'
import AppNavigation from '../Navigation/AppNavigation'
import { createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers'

const navReducer = (state, action) => {
  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
}

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    nav: navReducer
  })
  return createStore(rootReducer, applyMiddleware(middleware))
}