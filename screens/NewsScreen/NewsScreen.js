//NewsScreen
import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper'
import { DrawerLabel } from '../../Navigation/DrawerLabel';

export class NewsScreen extends Component {

    static navigationOptions = {
        drawerLabel: (
          <DrawerLabel
            label = {strings.newsTitle}
            icon = {images.menuNewsIcon.uri}
          />
        ),
    }

    render() {
      return (
        <View style={styles.container}>
          <Text>I am Signup Screen</Text>
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
  
  export default NewsScreen;