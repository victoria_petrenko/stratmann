//ContactsScreen.js

import React, { Component } from 'react';

//Modules
import {
  View,
  Text,
  Alert,
  StyleSheet,
} from 'react-native';
import InputScrollView from 'react-native-input-scroll-view';
import { DrawerActions } from "react-navigation";

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper'

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from '../../Navigation/NavigationView';
import GreyTextInput from '../../components/GreyTextInput';
import GradientButton from "../../components/GradientButton";

export class ContactsScreen extends Component {

  static navigationOptions = {
    drawerLabel: (
      <DrawerLabel
        label={strings.contactTitle}
        icon={images.menuContactIcon.uri}
        useSeparator={true}
      />
    ),
  }

  //Props
  constructor(props) {
    super(props);

    this.message = null;
    this.phoneNumber = null;

    this.state = {
      message: this.message,
      phoneNumber: this.phoneNumber,
    };
  }

  //Private Methods
  showAlert(title, message, cancelTitle) {

    Alert.alert(
      title,
      message,
      [
        { text: cancelTitle, onPress: () => this.closeScreen(), style: 'cancel' }
      ],
      { cancelable: false }
    )
  }

  closeScreen() {

    if (!this.state.message) {
      return
    }

    const { navigation } = this.props

    if (typeof navigation !== "undefined") {
      navigation.navigate("HomeScreen");
    }
  }

  //Actions
  onPressSendButton() {

    if (!this.state.message) {
      this.showAlert("", strings.messageIsRequiredText, strings.okButtonTitle);
      return
    }
    this.showAlert("", strings.acceptedApplication, strings.okButtonTitle);
  }

  //Render
  render() {
    return (
      <View style={styles.container}>

        <NavigationView
          leftButton={images.menuIcon.uri}
          titleLabel={strings.contactTitle}
          onPressLeftButton={() => {
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
            } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
            }
          }}
        />

        <View style={styles.mainView}>

          <InputScrollView>

            <Text style={styles.messageText}>
              {strings.messageRequiredText}
            </Text>

            <GreyTextInput
              placeholder={strings.writeYourMessagePlaceholder}
              onChangeText={(text) => this.setState({ message: text })}>
            </GreyTextInput>

            <Text style={styles.messageText}>
              {strings.phoneNumberText}
            </Text>

            <GreyTextInput
              placeholder={strings.leaveYourPhoneNumberPlaceholder}
              onChangeText={(text) => this.setState({ phoneNumber: text })}
              keyboardType={'number-pad'}>
            </GreyTextInput>

            <GradientButton
              title={strings.sendButtonTitle}
              onPress={this.onPressSendButton.bind(this)}>
            </GradientButton>

          </InputScrollView>
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#6753D5",
  },
  mainView: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
  },
  messageText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: "left",
    marginLeft: 16,
    marginTop: 20,
  },
})

export default ContactsScreen;