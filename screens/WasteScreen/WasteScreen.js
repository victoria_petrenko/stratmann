//WasteScreen
import React, { Component } from 'react';
import { View, 
        StyleSheet, 
        FlatList } from 'react-native';

//Modules
import { SearchBar } from 'react-native-elements';
import ViewOverflow from 'react-native-view-overflow';
import { createFilter } from 'react-native-search-filter';
import { DrawerActions } from "react-navigation";

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from "../../Navigation/NavigationView";
import LocationsListRow from "../LocationsListScreen/LocationsListRow";
import WasteRow from './WasteRow';

//Services
import GarbageService from '../../services/GarbageService';


const KEYS_TO_FILTERS = ['descriptionTitle'];

export class WasteScreen extends Component {

    static navigationOptions = {
        drawerLabel: (
          <DrawerLabel
            label = {strings.wasteTitle}
            icon = {images.menuWasteIcon.uri}
          />
        ),
    }

    constructor(props) {
      super(props);
  
      this.isContainerServiceWaste = false;

      this.searchBar = null;
      this.isSearch = false;
      this.searchQuery = "";
      this.wastes = GarbageService.sharedInstance().state.garbageTypeDetailsData,

      this.state = {
        searchBar: this.searchBar,
        isSearch: this.isSearch,
        wastes: this.wastes,
      };

      if (this.props.navigation.state.params) {
        if (typeof this.props.navigation.state.params.isContainerServiceWaste  !== "undefined") {
          this.isContainerServiceWaste = this.props.navigation.state.params.isContainerServiceWaste;
        }
      }
    }

    //Private
    //Search
    setSearchBar(ref) {

      if (ref != null && this.searchBar == null) {
  
        this.searchBar = ref;
        this.setState({
          searchBar: this.searchBar,
        });
      }
    }
  
    isActiveSearch = (isActive, canceled) => {
  
      if (this.isSearch == isActive) {
        return;
      }
  
      this.isSearch = isActive;
      this.setState({
        isSearch: this.isSearch,
      });
  
      if (!this.isSearch && !isActive && !canceled) {
        // this.searchBar.onCancel();
      }
    }

    //Actions
    showCalendarScreen = (garbage) => {
      const { navigation } = this.props
  
      if (typeof navigation !== "undefined") {
        navigation.navigate('GarbageRemovalScreen', { selectedDetailGarbage: garbage })
      }
    }

    onPressSelectedWaste = (garbage) => {
      const { navigation } = this.props
  
      if (typeof navigation !== "undefined") {
        navigation.navigate("ContainerServicesScreen", { selectedWaste: garbage });
      }
    }

    //Search
    handleSearchQueryChange = (query, canceled) => {

      var isCanceled = canceled ? canceled : false;
  
      this.searchQuery = query || "";
  
      this.setState({
        searchQuery: this.searchQuery,
      });
      this.isActiveSearch(query.length > 0, isCanceled);
    }
  
    handleSearchCancel = () => {
      this.handleSearchQueryChange("", true);
    }
  
    handleSearchClear = () => {
      this.handleQueryChange("", true);
    }


    render() {

      const filteredWastes = this.isSearch ? this.state.wastes.filter(createFilter(this.state.searchQuery, KEYS_TO_FILTERS)) : this.state.wastes;

      return (
        <View style={styles.container}>

        <NavigationView
          leftButton={ this.isContainerServiceWaste ? images.menuBackIcon.uri : images.menuIcon.uri}
          titleLabel={strings.wasteTitle}
          onPressLeftButton={() => {

            if (this.isContainerServiceWaste) {
              this.props.navigation.goBack();
              return;
            }
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
          } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
          }
          }}
        />

        <SearchBar
          containerStyle={styles.searchBarViewContainer}
          inputStyle={styles.searchBarTextInput}
          ref={search => this.setSearchBar(search)}
          autoFocus={this.isSearch}
          platform="ios"
          searchIcon
          clearIcon={this.isSearch}
          placeholderTextColor='#8D7EE0'
          placeholder={strings.searchTitle}
          onChangeText={this.handleSearchQueryChange}
          onCancel={this.handleSearchCancel}
          onClear={this.handleSearchClear}
          value={this.searchQuery}>
        </SearchBar>


        <View style={styles.searchSeparatorView}>
          <ViewOverflow style={styles.searchSeparatorContainer}>
          </ViewOverflow>
        </View>

        <FlatList style={styles.garbageFlatList}
          data={filteredWastes}
          renderItem={(data) => <WasteRow {...data}
          onPressItem={(item) => this.isContainerServiceWaste ? this.onPressSelectedWaste(item) : this.showCalendarScreen(item)} />}
        />

      </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column",
      backgroundColor: "#6753D5",
    },
    searchBarViewContainer: {
      backgroundColor: "#6753D5",
      borderBottomColor: 'transparent',
      borderTopColor: 'transparent',
      height: 44,
    },
    searchBarTextInput: {
      borderTopColor: 'transparent',
      backgroundColor: '#6753D5',
      borderBottomColor: '#8D7EE0',
    },
    searchSeparatorView: {
      backgroundColor: '#6753D5',
      height: 8,
      width: '100%',
      flexDirection: "column",
      alignSelf: 'center',
    },
    searchSeparatorContainer: {
      backgroundColor: '#8D7EE0',
      height: 1,
      width: '92%',
      flexDirection: "column",
      alignSelf: 'center',
    },
    garbageFlatList: {
      flexDirection: "column",
      backgroundColor: '#fff',
      marginTop: 10,
    }
  })

  export default WasteScreen;