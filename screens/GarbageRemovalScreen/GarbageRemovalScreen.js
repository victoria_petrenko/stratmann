//GarbageRemovalScreen
import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
  PropTypes
} from "react-native";

//Modules
import { DrawerActions } from "react-navigation";
import { CalendarList } from "react-native-calendars";
import XDate from "xdate";
import { LocaleConfig } from "react-native-calendars";
import DeviceInfo from "react-native-device-info";
import ViewOverflow from 'react-native-view-overflow';
import { createFilter } from 'react-native-search-filter';

//Helpers
import DeviceHelper from '../../Helpers/Device+Helper';
import { images } from "../../Helpers/Image+Helper";
import { strings } from "../../Helpers/Localization+Helper";

//Services
import GarbageService from "../../services/GarbageService";
import GarbageRemovalRow from "./GarbageRemovalRow"

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from "../../Navigation/NavigationView";

//Calendar Styles
const calendarDotDateContainer = {
  container: {
    flexDirection: "column",
    backgroundColor: "red",
    borderRadius: 2.5,
    height: 28,
    bottom: -28
  },
  text: {
    color: "white",
    height: 28,
    top: -28,
    flexDirection: "column",
  }
};

const calendarSelectedDateContainer = {
  container: {
    backgroundColor: "white",
    borderRadius: 3,
    height: 28
  },
  text: {
    color: "red"
  }
};

const calendarStyles = StyleSheet.create({
  header: {
    backgroundColor: "red",
    paddingVertical: 10
  },
  week: {
    backgroundColor: "black",
    marginTop: 0,
    paddingBottom: 10
  },
  monthText: {
    fontSize: 24,
    lineHeight: 30,
    margin: 0,
    letterSpacing: 1.45,
    color: "red"
  },
  dayHeader: {
    fontSize: 18.2,
    color: "blue"
  },
  day: {
    justifyContent: "center"
  },
  dayText: {
    fontSize: 18.2,
    color: "white",
    marginTop: 0
  },
  todayText: {
    fontSize: 18.2,
    color: "black",
    marginTop: 0
  },
  disabledText: {
    opacity: 0.5,
    fontSize: 18.2,
    color: "red"
  }
});

export class GarbageRemovalScreen extends Component {
  //Navigation
  static navigationOptions = {
    drawerLabel: (
      <DrawerLabel
        label={strings.garbageRemovalTitle}
        icon={images.menuCalendarIcon.uri}
      />
    )
  };

  //Actions
  onPressSettingsButton = () => {
    this.props.navigation.push("ToolsScreen");
  };

  onPressTimelineButton = () => {
    this.props.navigation.push("GarbageTimeLineScreen");
  };

  //Methods
  configureCalendarLanguage() {
    LocaleConfig.locales["en"] = {
      monthNames: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ],
      monthNamesShort: [
        "Jan.",
        "Feb.",
        "Mar.",
        "Apr.",
        "May.",
        "June",
        "July",
        "Aug.",
        "Sept.",
        "Oct.",
        "Nov.",
        "Dec."
      ],
      dayNames: [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      ],
      dayNamesShort: ["S", "M", "T", "W", "T", "F", "S"]
    };

    LocaleConfig.locales["de"] = {
      monthNames: [
        "Januar",
        "Februar",
        "März",
        "April",
        "Mai",
        "Juni",
        "Juli",
        "August",
        "September",
        "Oktober",
        "November",
        "Dezember"
      ],
      monthNamesShort: [
        "Jän.",
        "Feb.",
        "März",
        "Apr.",
        "Mai",
        "Juni",
        "Juli",
        "Aug.",
        "Sept.",
        "Okt.",
        "Nov.",
        "Dez."
      ],
      dayNames: [
        "Sonntag",
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag"
      ],
      dayNamesShort: ["S", "M", "D", "M", "D", "F", "S"]
    };

    LocaleConfig.locales["ru"] = {
      monthNames: [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь"
      ],
      monthNamesShort: [
        "Янв.",
        "Февр.",
        "Mарт",
        "Aпр.",
        "Mай",
        "Июнь",
        "Июль",
        "Авг.",
        "Сент.",
        "Окт.",
        "Ноябрь",
        "Дек."
      ],
      dayNames: [
        "Воскресенье",
        "Понедельник",
        "Вторник",
        "Среда",
        "Четверг",
        "Пятница",
        "Суббота"
      ],
      dayNamesShort: ["В", "П", "В", "С", "Ч", "П", "С"]
    };
    LocaleConfig.defaultLocale = DeviceHelper.sharedInstance().getDeviceLocale();
  }

  constructor(props) {
    super(props);

    this.selectedInfoDayData = this.getSelectedInfoDayData();
    this.selectedDay = XDate();
    this.currentMonth = null;

    this.state = {
      currentMonth: this.currentMonth,
      selectedDay: this.selectedDay,
      selectedInfoDayData: this.selectedInfoDayData,
      calendarDates: GarbageService.sharedInstance().state.garbageDates,
      calendarInfoDates: GarbageService.sharedInstance().state.garbageInfoData,
      markedData: this.props.markedData,
      selectedDetailGarbage: this.selectedDetailGarbage,
    };

    if (this.props.navigation.state.params) {
      if (typeof this.props.navigation.state.params.selectedDetailGarbage  !== "undefined") {
        this.selectedDetailGarbage = this.props.navigation.state.params.selectedDetailGarbage;
        this.updateSelectedDayForDetailGarbage(this.selectedDetailGarbage)
      }
    }
  }

  getSelectedInfoDayData = () => {
    var selectedData;
    if (this.selectedInfoDayData) {
      selectedData = this.selectedInfoDayData;
    } else {
      selectedData = GarbageService.sharedInstance().selectedInfoDataForDate(XDate());
    }
    return selectedData;
  }

  //Methods
  //Life-Cycle
  componentWillUnmount() {    
    this.garbage = null;

    this.selectedInfoDayData = null;
    this.selectedDay = null;
    this.currentMonth = null;
    this.props.navigation.state.params = null;

    this.setState({ selectedInfoDayData: this.selectedInfoDayData, 
                    selectedDay: this.selectedDay, 
                    currentMonth: this.currentMonth,
                    selectedDetailGarbage: null});
  }

  //Private
  calendarDates = () => {

    var dates = new Array();

    var randomDate = require('random-datetime');
    var currentDate = XDate()
    for (month = 1; month < 13; month++) {

      for (index = 0; index < 10; index++) {

        var randomDay = randomDate({
          year: currentDate.getFullYear(),
          month: month
        });

        dates.push(randomDay);
      }
    }

    this.selectedDay = currentDate;
    this.setState({ selectedDay: currentDate });
    return dates;
  };

  dateText = (checkedDate) => {
    var date = checkedDate;
    if (!(date instanceof Date) && !(date instanceof XDate)) {
      date = new XDate(date.dateString);
    }
    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  }

  markedData = () => {

    var markedData = []
    for (index in this.state.calendarDates) {

      var date = this.state.calendarDates[index];
      var dateText = this.dateText(date);

      markedData.push(dateText);
    }
    return markedData;
  }

  markedDates = () => {

    var markedDataAray = [];
    if (this.state.markedData) {
      markedDataAray = this.state.markedData;
    } else {
      markedData = this.markedData();
      this.setState({ markedData: markedData });
      markedDataAray = markedData;
    }

    var currentDateText = this.dateText(XDate());
    markedDataAray.push(currentDateText);

    let dates = {};
    markedDataAray.forEach((val) => {
      var selected = currentDateText == val;

      dates[val] = selected ? { selected: true, marked: true } : { marked: true, dotColor: '#FF4B71', activeOpacity: 0 };
    });
    return dates;
  };

  updateSelectedDay = day => {
    this.selectedDay = this.getDate(day);
    this.setState({ selectedDay: this.selectedDay });
    var selectedDayData = GarbageService.sharedInstance().selectedInfoDataForDate(day);
    this.selectedInfoDayData = selectedDayData;
    this.setState({ selectedInfoDayData: this.selectedInfoDayData })
  };

  updateCurrentMonth = (month) => {
    this.currentMonth = month;
    this.setState({ currentMonth: this.currentMonth });
  };

  updateSelectedDayForDetailGarbage = (detailGarbage) => {

    if (!detailGarbage) {
      return;
    }
    var selectedDay = GarbageService.sharedInstance().selectedDayForDetailGarbage(detailGarbage);
    this.updateCurrentMonth(selectedDay);
    this.updateSelectedDay(selectedDay);
    this.getSelectedInfoDayData();
  }

  selectedMonthTitle = () => {
    var month = this.getDate(this.currentMonth);
    var monthText = month.toString("MMM");
    return monthText;
  };

  selectedYearTitle = () => {
    var year = this.getDate(this.currentMonth);
    var yearText = year.toString("yyyy");
    return yearText;
  };

  calendarScrollRange = (past) => {
    var currentMonth = XDate().getMonth() + 1;
    var monthIndex = past ? 1 : 0;
    var scrollRange = past ? 12 - (12 - currentMonth) - monthIndex : (12 - currentMonth) + monthIndex ;
    return scrollRange;
  };

  getMinAndMaxCalendarDates() {
    var date = new Date();
    var firstYearDay = new Date(date.getFullYear(), 0, 1);
    var lastYearDay = new Date(date.getFullYear(), 12, 0);

    var firstYearDayText = this.dateText(firstYearDay);
    var lastYearDayText = this.dateText(lastYearDay);

    return {minDate: firstYearDayText, maxDate: lastYearDayText};
  }

  _renderArrow = (direction) => {
    if(direction === 'left') {
        return <Text>{this.state.previousMonth}</Text>
    } else {
        return <Text>{this.state.nextMonth}</Text>
    }
}

getDate = (checkedDate) => {
    
  var date;
  if (checkedDate == null) {
    date = XDate();
  } else if (!(checkedDate instanceof Date)) {
    date = new XDate(checkedDate.dateString);
  } else {
    date = new XDate(checkedDate);
  }
  return date;
}

  render() {
    this.configureCalendarLanguage();
    let minMaxCalendarDates = this.getMinAndMaxCalendarDates();

    return (
      <View style={styles.container}>
        <NavigationView
          leftButton={images.menuIcon.uri}
          rightButton={images.bagIcon.uri}
          titleLabel={strings.garbageRemovalTitle}
          onPressLeftButton={() => {
            this.props.navigation.navigate("GarbageRemovalScreen");
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
            } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
              this.componentWillUnmount();
            }
          }}
          onPressRightButton={this.onPressSettingsButton}
        />
        <View style={styles.calendarHeaderContainer}>
          <View style={styles.calendarMonthContainer}>
            <Text style={styles.calendarMonthText}>
              {this.selectedMonthTitle()}
            </Text>
            <Text style={styles.calendarDateText}>
              {this.selectedYearTitle()}
            </Text>
            <TouchableOpacity
              style={styles.calendarTimelineContainer}
              onPress={this.onPressTimelineButton}
            >
              <Image resizeMode="contain" source={images.timelineIcon.uri} />
            </TouchableOpacity>
          </View>
        </View>
        <CalendarList
          style={{
            height: 200,
          }}
          renderArrow={this._renderArrow}
          theme={{
            calendarBackground: "#6753D5",
            selectedDayBackgroundColor: "#ffffff",
            selectedDayTextColor: "#FF4B71",
            todayTextColor: "#00adf5",
            dayTextColor: "white",
            dotColor: "red",
            selectedDotColor: "#ffffff",
            monthTextColor: "white",
            textDayFontFamily: "helvetica",
            textMonthFontFamily: "helvetica",
            textDayFontSize: 16,
            textMonthFontSize: 30,
            textDayHeaderFontFamily: "helvetica",
            textDayHeaderFontSize: 16,
            "stylesheet.calendar.header": {
              header: { height: 0 },
              week: {
                marginTop: 5,
                flexDirection: "row",
                justifyContent: "space-between"
              },
              day: {
                fontSize: 16,
                lineHeight: 18,
                margin: 0,
                letterSpacing: 1.45
              }
            }
          }}
          markedDates={this.markedDates()}
          // markingType= "custom"
          // Initially visible month. Default = Date()
          current={this.dateText(this.selectedDay)}
          // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
          minDate={minMaxCalendarDates.minDate}
          // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
          maxDate={minMaxCalendarDates.maxDate}
          // Handler which gets executed on day press. Default = undefined
          onDayPress={day => {
            console.log("selected day", day);
            this.updateSelectedDay(day);
          }}
          // Handler which gets executed on day long press. Default = undefined
          onDayLongPress={day => {
            console.log("selected day", day);
          }}
          onVisibleMonthsChange={(months) => {
            if (months.length == 1 && this.currentMonth != months[0]) {
              var month = months[0];
              this.updateCurrentMonth(month);
            }
            console.log('now these months are visible', months);
          }}
          // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
          monthFormat={"MMM yyyy"}
          // Handler which gets executed when visible month changes in calendar. Default = undefined
          scrollToMonth={month => {
            var updatedMonth = month;
            this.updateCurrentMonth(updatedMonth);
          }}

          onMonthChange={month => {
            console.log("month changed", month);
            var updatedMonth = month;
            this.updateCurrentMonth(updatedMonth);
          }}
          // Enable or disable vertical scroll indicator. Default = false
          showScrollIndicator={true}
          // Enable horizontal scrolling, default = false
          horizontal={true}
          // Enable paging on horizontal, default = false
          pagingEnabled={true}
          // Replace default arrows with custom ones (direction can be 'left' or 'right')
          renderArrow={direction => {
            if (direction == "left")
              return (
                <View style={styles.calendarMonthContainer}>
                  <Image
                    style={{ marginLeft: -10 }}
                    source={images.leftArrowIcon.uri}
                  />
                  <Text style={styles.calendarMonthText}>
                    {this.selectedMonthTitle()}
                  </Text>
                  <Text style={styles.calendarDateText}>
                    {this.selectedYearTitle()}
                  </Text>
                </View>
              );
            if (direction == "right")
              return (
                <View style={styles.calendarMonthContainer}>
                  <Image
                    style={{ marginRight: -10 }}
                    source={images.rightArrowIcon.uri}
                  />
                </View>
              );
          }}
          // Max amount of months allowed to scroll to the past. Default = 50
          pastScrollRange={this.calendarScrollRange(true)}
          // Max amount of months allowed to scroll to the future. Default = 50
          futureScrollRange={this.calendarScrollRange(false)}
          // futureScrollRange={4}
          // Do not show days of other months in month page. Default = false
          hideExtraDays={true}
          // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
          firstDay={1}
          // Hide day names. Default = false
          hideDayNames={false}
          // Show week numbers to the left. Default = false
          showWeekNumbers={false}
          // Handler which gets executed when press arrow icon left. It receive a callback can go back month
          onPressArrowLeft={substractMonth => substractMonth()}
          // Handler which gets executed when press arrow icon left. It receive a callback can go next month
          onPressArrowRight={addMonth => addMonth()}
        />
        <ViewOverflow style={styles.notificationsContainer}>
          <FlatList style={styles.flatListContainer}
            data={this.getSelectedInfoDayData()}
            renderItem={(data) => <GarbageRemovalRow {...data} />}
          />
        </ViewOverflow>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#6753D5"
  },
  navigationContainer: {
    backgroundColor: "#6753D5"
  },
  calendarHeaderContainer: {
    flexDirection: "column",
    height: 64
  },
  calendarMonthContainer: {
    flexDirection: "row",
    alignItems: "center",
    margin: 10
  },
  calendarMonthText: {
    color: "#fff",
    fontFamily: "helvetica",
    fontSize: 30,
    textAlign: "left",
    flexDirection: "row",
    marginLeft: 25,
    alignItems: "center"
  },
  calendarDateText: {
    color: "rgba(255, 255, 255, 0.5)",
    fontFamily: "helvetica",
    fontSize: 30,
    textAlign: "left",
    flexDirection: "row",
    marginLeft: 15,
    top: 0
  },
  calendarTimelineContainer: {
    flex: 3,
    flexDirection: "row",
    marginRight: 15,
    top: -3,
    justifyContent: "flex-end"
  },
  calendarContainer: {
    flexDirection: "column",
    height: 280
  },
  flatListContainer: {
    marginTop: -18,
  },
  notificationsContainer: {
    flex: 2,
    flexDirection: "column",
    backgroundColor: "white",
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});

export default GarbageRemovalScreen;
