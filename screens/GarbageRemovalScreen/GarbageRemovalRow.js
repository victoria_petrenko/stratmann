//GarbageRemovalRow

import React from 'react';
//Modules
import { View, Text, StyleSheet, Dimensions, } from 'react-native';

const GarbageRemovalRow = (props) => (
  <View style={styles.container}>
    <View style={styles.rowContainer}>
      <Text
        style={styles.text}
        numberOfLines={10} >
        {`${props.item.garbageTypeText} (${props.item.garbageDescription.toLowerCase()}), \n${props.item.dateText}`}
      </Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 10,
  },
  rowContainer: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: Dimensions.get("window").width - 32,
    shadowOffset: { x: 0, y: 2 },
    shadowColor: 'rgba(0, 0, 0, .12)',
    shadowRadius: 5,
    shadowOpacity: 1.0,
    justifyContent: "center",
    elevation: 1,
  },
  text: {
    fontSize: 15,
    color: 'rgba(155, 155, 155, 1)',
    fontFamily: 'helvetica',
    textAlign: 'left',
    marginLeft: 16,
  },
});

export default GarbageRemovalRow;