//AboutUsScreen.js
import React, { Component } from 'react';

//Modules
import {
  View,
  Text,
  StyleSheet,
  FlatList,
} from 'react-native';
import { DrawerActions } from "react-navigation";

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';
import DeviceHelper from '../../Helpers/Device+Helper';

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from '../../Navigation/NavigationView';

const CONTAINER_ROW_HEIGHT = 40;

export class AboutUsScreen extends Component {

  static navigationOptions = {
    drawerLabel: (
      <DrawerLabel
        label={strings.aboutUsTitle}
        icon={images.menuAboutIcon.uri}
      />
    ),
  }

  //Props
  constructor(props) {
    super(props);

    this.flatListData = this.getAboutUsData();
    this.appVersion = DeviceHelper.sharedInstance().getVersionNumber();

    this.state = {
      flatListData: this.flatListData,
      appVersion: this.appVersion,
    };
  }

  //Private Methods
  getAboutUsData = () => {

    var flatListData = []

    for (index = 0; index < 4; index++) {

      var rowTitle = "";
      var icon = null;

      switch (index) {
        //Home Menu
        case 0: {
          rowTitle = strings.companyProfile;
          icon = images.menuHomeIcon.uri;
          break;
        }
        //Bussiness Menu
        case 1: {
          rowTitle = strings.bussinessArreaTitle;
          icon = images.otherProfileIcon.uri;
          break;
        }
        //Impressum Menu
        case 2: {
          rowTitle = strings.impressumTitle;
          icon = images.impressumIcon.uri;
          break;
        }
        //Data protection
        case 3: {
          rowTitle = strings.dataProtectionTitle;
          icon = images.dataProtectionIcon.uri;
          break;
        }
      }

      var flatListRow = {
        image: icon,
        title: rowTitle,
      };

      flatListData.push(flatListRow);
    }
    return flatListData;
  }

  //Render
  render() {
    return (
      <View style={styles.container}>

        <NavigationView
          leftButton={images.menuIcon.uri}
          titleLabel={strings.aboutUsTitle}
          onPressLeftButton={() => {
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
            } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
            }
          }}
        />

        <View style={styles.mainView}>

          <View style={{
            height: CONTAINER_ROW_HEIGHT * this.state.flatListData.length + 30,
            flexDirection: "column", marginTop: 10,
          }}>
            <FlatList style={{ flex: 1 }}
              scrollEnabled={false}
              data={this.state.flatListData}
              renderItem={(data) =>
                <DrawerLabel
                  label={data.item.title}
                  icon={data.item.image}
                  //Use separator for last row
                  useSeparator={data.index == 3}
                  smallRow={true}
                />}
            >
            </FlatList>
          </View>

          <View style={{ height: 31, marginLeft: 23 }}>
            <Text style={styles.appVersionLabelContainer}>
              {`Version: ${this.state.appVersion}`}
            </Text>
          </View>

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#6753D5",
  },
  mainView: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
  },
  appVersionLabelContainer: {
    color: '#8A80BB',
    fontFamily: "helvetica",
    fontSize: 13,
    textAlign: 'left'
  },
})

export default AboutUsScreen;