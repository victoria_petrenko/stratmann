//ToolsScreen
import React, { Component } from 'react';
//Modules
import {
  View,
  Text,
  StyleSheet,
  FlatList,
} from 'react-native';
import { DrawerActions } from "react-navigation";
import InputScrollView from 'react-native-input-scroll-view';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';

//Components
import { NavigationView } from "../../Navigation/NavigationView";
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import ToolsRow from './ToolsRow'

export class ToolsScreen extends Component {

  static navigationOptions = {
    drawerLabel: (
      <DrawerLabel
        label={strings.toolsTitle}
        icon={images.menuToolsIcon.uri}
      />
    ),
  }

  //Props
  constructor(props) {
    super(props);

    this.sumMessage = null;
    this.toolsData = this.toolsData();
    this.length = null;
    this.width = null;
    this.height = null;

    this.state = {
      sumMessage: this.sumMessage,
      toolsData: this.toolsData,
      length: this.length,
      width: this.width,
      height: this.height,
    };
  }

  toolsData() {
    var tools = []

    var length = { title: strings.numberOfPiecesTitle, value: this.length, index: 0 };
    tools.push(length);

    var width = { title: strings.numberOfPiecesTitle, value: this.width, index: 1 };
    tools.push(width);

    var height = { title: strings.numberOfPiecesTitle, value: this.height, index: 2 };
    tools.push(height);
    return tools
  }; 

  //Render
  render() {
    return (
      <View style={styles.container}>

        <NavigationView
          leftButton={images.menuBackIcon.uri}
          titleLabel={strings.toolsTitle}
          onPressLeftButton={() => {
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
            } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
            }
          }}
        />

        <View style={styles.contentCotainer}>
          <InputScrollView>

            <Text style={styles.calculatorText}>
              {strings.calculatorTitle}
            </Text>

            <Text style={styles.calculatorLabel}>
              {this.state.sumMessage}
            </Text>

            <Text style={styles.calculatorText}>
              {strings.cubCalculatorTitle}
            </Text>

            <View style={styles.separator}>
            </View>

             <View style={styles.flatListContainer}>
                  <FlatList style={styles.toolsFlatList}
                    scrollEnabled={false}
                    data={this.state.toolsData}
                    extraData={this.state}
                    renderItem={(data) => <ToolsRow {...data}
                      onPressItem={(item) => this.showSettingsItem(item)}
                      onChangeItemText={(text) => this.updateNumberOfPiecesValue(text)} />}
                  />
                </View>

                <View style={styles.separator}>
            </View>
          </InputScrollView>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#6753D5",
    flexDirection: "column",
  },
  contentCotainer: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
  },
  calculatorText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 17,
    textAlign: "left",
    marginLeft: 40,
    marginTop: 20,
  },
  calculatorLabel: {
    backgroundColor: '#EFEFF4',
    marginHorizontal: 25,
    marginTop: 20,
    height: 44,
  },
  separator: {
    backgroundColor: "#E1E1E1",
    height: 1,
    marginTop: 14,
  },
  flatListContainer: {
    flexDirection: "column",
    height: 174,
  },
  toolsFlatList: {
    flexDirection: "column",
  },
})

export default ToolsScreen;