//HomeScreen
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from "react-native";
import XDate from "xdate";

//Components
import { DrawerActions } from "react-navigation";
import LinearGradient from "react-native-linear-gradient";
import { images } from "../../Helpers/Image+Helper";
import { strings } from "../../Helpers/Localization+Helper";
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from "../../Navigation/NavigationView";

//Services
import GarbageService from '../../services/GarbageService'

export class HomeScreen extends Component {
  //Navigation
  static navigationOptions = {
    drawerLabel: (
      <DrawerLabel label={strings.homeTitle} icon={images.menuHomeIcon.uri} />
    )
  };

  constructor(props) {
    super(props);

    this.state = {
      currentDayScheduleText: this.scheduleText(),
    };
  }


  //Methods
  scheduleText = () => {
    let scheduleData = GarbageService.sharedInstance().selectedInfoDataForDate(XDate())
    var scheduleInfo;
    if (scheduleData.length > 0) {
      scheduleInfo = scheduleData[0]
    }
    
    return scheduleInfo
  }

  //Actions
  onPressGarbageRemovalButton = () => {
    this.props.navigation.navigate("GarbageRemovalScreen");
  };

  onPressLocationsButton = () => {
    this.props.navigation.navigate("LocationsScreen");
  };

  onPressWasteButton = () => {
    this.props.navigation.navigate("WasteScreen");
  };

  onPressContainerButton = () => {
    this.props.navigation.navigate("ContainerServicesScreen");
  };

  onPressToolsButton = () => {
    this.props.navigation.navigate("ToolsScreen");
  };

  render() {
    return (
      <LinearGradient
        colors={["#5A36D9", "rgba(90, 54, 217, 0.7)"]}
        style={styles.linearGradient}
      >
        <NavigationView
          backgroundColor="rgba(90, 54, 217, 0.0)"
          leftButton={images.menuIcon.uri}
          rightButton={images.bagIcon.uri}
          titleLabel={strings.homeTitle}
          onPressLeftButton={() => {
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
            } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
            }
          }}
          onPressRightButton={this.onPressToolsButton}
        />
        <ScrollView
          contentContainerStyle={styles.scrollContainer}
          scrollEnabled={
            Dimensions.get("window").height - 64 > 600 ? false : true
          }
        >
          <View style={styles.logoContainer}>
            <Image source={images.logoIcon.uri} />
            <View style={styles.reminderContainer}>
              <View style={styles.timeContainer}>
                <View style={styles.timeImageContainer}>
                  <Image source={images.timeIcon.uri} />
                  <Text style={styles.timeText}>
                    {strings.nextPickupDateTitle}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.notificationsContainer}>
              <Text style={styles.notificationText}>
              {`${this.state.currentDayScheduleText.dateText} - ${this.state.currentDayScheduleText.garbageDescription}`}
              </Text>
            </View>
            <View style={styles.notificationsContainer}>
              <View style={styles.buttonsContainer}>
                <TouchableOpacity
                  style={styles.buttonContainerRow}
                  onPress={this.onPressGarbageRemovalButton}
                >
                  <View style={styles.imageContainerRow}>
                    <Image source={images.calendarIcon.uri} />
                    <Text style={styles.buttonText}>
                      {strings.garbageRemovalTitle}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.buttonContainerRow}
                  onPress={this.onPressLocationsButton}
                >
                  <View style={styles.imageContainerRow}>
                    <Image source={images.locationIcon.uri} />
                    <Text style={styles.buttonText}>
                      {strings.locationsTitle}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.buttonsContainer}>
                <TouchableOpacity
                  style={styles.buttonContainerRow}
                  onPress={this.onPressWasteButton}
                >
                  <View style={styles.imageContainerRow}>
                    <Image source={images.wasteIcon.uri} />
                    <Text style={styles.buttonText}>{strings.wasteTitle}</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.buttonContainerRow}
                  onPress={this.onPressContainerButton}
                >
                  <View style={styles.imageContainerRow}>
                    <Image source={images.containerIcon.uri} />
                    <Text style={styles.buttonText}>
                      {strings.containerServiceTitle}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    alignItems: "center",
    width: Dimensions.get("window").width,
    height:
      Dimensions.get("window").height - 64 > 600
        ? Dimensions.get("window").height - 64
        : 600
  },
  linearGradient: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    width: Dimensions.get("window").width
  },
  logoContainer: {
    position: "absolute",
    alignItems: "center",
    top: 21,
    flexDirection: "column",
    height: 38,
    width: 200
  },
  reminderContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    top: 56,
    backgroundColor: "#9889EF",
    height: 50,
    width: Dimensions.get("window").width - 28,
    borderRadius: 5
  },
  timeContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  timeImageContainer: {
    flex: 1,
    marginLeft: 15,
    width: 20,
    flexDirection: "row"
  },
  timeText: {
    color: "#fff",
    fontFamily: "helvetica",
    fontSize: 13,
    textAlign: "left",
    flexDirection: "row",
    marginLeft: 14,
    top: 4
  },
  buttonText: {
    color: "#fff",
    fontFamily: "helvetica",
    fontSize: 13,
    textAlign: "left",
    flexDirection: "row",
    top: 27
  },
  notificationsContainer: {
    flexDirection: "column",
    top: 56,
    width: Dimensions.get("window").width - 28,
    borderRadius: 5
  },
  notificationText: {
    color: "#fff",
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: "left",
    flexDirection: "column",
    marginLeft: 10,
    top: 10,
    height: 50
  },
  buttonsContainer: {
    flexDirection: "row",
    width: Dimensions.get("window").width - 28,
    borderRadius: 5
  },
  buttonContainerRow: {
    flex: 3,
    flexDirection: "row",
    flexWrap: "wrap",
    borderRadius: 5,
    height: 164,
    backgroundColor: "#9889EF",
    width: Dimensions.get("window").width / 2,
    justifyContent: "space-between",
    margin: 7.5
  },
  imageContainerRow: {
    position: "relative",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    flexGrow: 1
  }
});

export default HomeScreen;
