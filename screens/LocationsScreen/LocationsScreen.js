//LocationsScreen
import React, { Component } from 'react';

//Modules
import {
    View,
    StyleSheet,
    Text,
    ImageBackground,
    Dimensions,
    Platform,
    Image,
    TouchableOpacity,
    Alert,
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Orientation from 'react-native-orientation';
import * as Animatable from 'react-native-animatable';
import { EventRegister } from 'react-native-event-listeners'
import { OpenMapDirections } from 'react-native-navigation-directions';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';
import renderIf from '../../Helpers/renderIf';
import DeviceHelper from '../../Helpers/Device+Helper';
import { DrawerActions } from "react-navigation";

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from "../../Navigation/NavigationView";
import LocationMarkerCallout from './LocationMarkerCallout';
import CheckMark from '../../components/CheckMark';

//Services
import LocationsService from '../../services/LocationsService';
import DBService from '../../services/DBService';

const mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#f5f5f5"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#bdbdbd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e5e5e5"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dadada"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e5e5e5"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c9c9c9"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    }
];

var screenHeight = Dimensions.get("window").height;

export class LocationsScreen extends Component {

    static navigationOptions = ({ navigation }) => ({
        drawerLabel: (
            <DrawerLabel
                label={strings.locationsTitle}
                icon={images.menuLocationsIcon.uri}
            />
        ),
    }
    );

    constructor(props) {
        super(props);

        this.garbageMarkers = DBService.sharedInstance().getLocationsDataForPage(0);
        this.activeGarbageMarkers = [];
        this.showedOfficeMarkers = true;
        this.showedContainerMarkers = true;
        this.currentLatitude =  0,
        this.currentLongitude = 0,
        this.visibleMap = false;

        this.state = {
            garbageMarkers: this.garbageMarkers,
            isPortrait: Orientation.orientation == 'PORTRAIT' ? true : false,
            showedMoreActions: false,
            currentLatitude: this.currentLatitude,
            currentLongitude: this.currentLongitude,
            visibleMap: this.visibleMap,
            selectedMarker: false,
            visibleFilter: false,
            showedOfficeMarkers: this.showedOfficeMarkers,
            showedContainerMarkers: this.showedContainerMarkers,
            activeGarbageMarkers: this.activeGarbageMarkers,
        };
    }

    updateComponent = () => {
        this.updateVisibleMapState(true);
        this.loadNextGarbageLocationsPageWithDelay();
      }

    //Life-Cycle
    componentWillMount() {
        
        if (!this.visibleMap) {
            this.updateVisibleMapState(true);
        }

        var self = this;
        this.props.navigation.addListener(
            'willFocus',
            payload => {
                if (!self.visibleMap) {
                    self.updateComponent();
                }
            }
          );

          // Event Listener for orientation changes
        Dimensions.addEventListener('change', () => {
            screenHeight = Dimensions.get("window").height;
            this.setState({
                isPortrait: Orientation.orientation == 'PORTRAIT' ? true : false,
            });
        });

        EventRegister.addEventListener('changedDBLocationList', (keys) => {
            this.setState({
                // garbageMarkers: DBService.sharedInstance().getLocationsDataForPage(0),
            });
        });

        EventRegister.addEventListener('loadedNewLocationsDataPage', () => {
            if (this.visibleMap) {
                this.updateGarbageLocations();
                this.loadNextGarbageLocationsPageWithDelay();
            }
        });
    }

    componentDidMount() {
        
        this.loadNextGarbageLocationsPageWithDelay();

        EventRegister.addEventListener('changedCurrentLocation', (position) => {
            this.currentLatitude = position.coords.latitude;
            this.currentLongitude = position.coords.longitude;
            this.setState({
                currentLatitude: this.currentLatitude,
                currentLongitude: this.currentLongitude,
            });
        });

        LocationsService.sharedInstance().updateCurrentPosition();
    }

    async loadNextGarbageLocationsPageWithDelay() {

        setTimeout(function () {
            this.loadNextGarbageLocationsPage();
        }.bind(this), 2000);
    }

    updateisOfficeValue() {

        for (index = 0; index < locations.length; index++) {
            var location = locations[index];
            DBService.sharedInstance().addisOfficeLocation(location);
        }
    }

    updateGarbageLocations() {

        let newPage = DBService.sharedInstance().getCurrentGarbageLocationsPage();
        let newLocations = DBService.sharedInstance().getLocationsDataForPage(newPage);
        var currentLocations = this.garbageMarkers;
        this.garbageMarkers = currentLocations.concat(newLocations);
        this.setState({ garbageMarkers: currentLocations.concat(newLocations) });

        this.filterGarbageLocations(newLocations, false);
    }

    loadNextGarbageLocationsPage() {

        var nextPage = DBService.sharedInstance().getCurrentGarbageLocationsPage() + 1;
        DBService.sharedInstance().getLocationsDataForPage(nextPage);
    }

    filterGarbageLocations(garbageLocations, loadedAllGarbageLocations) {

        var locations = [];
        let filteredLocations = this.getFilteredLocations(garbageLocations);

        if (this.showedOfficeMarkers) {
            let offices = filteredLocations.offices;
            locations = locations.concat(offices);
        }

        if (this.showedContainerMarkers) {
            let containers = filteredLocations.containers;
            locations = locations.concat(containers);
        }

        if (loadedAllGarbageLocations) {
            this.activeGarbageMarkers = locations;
        } else {
            this.activeGarbageMarkers = this.activeGarbageMarkers.concat(locations);
        }
        this.setState({ activeGarbageMarkers: this.activeGarbageMarkers });
    }

    getFilteredLocations(garbageLocations) {

        var officeLocations = [];
        var containerLocations = [];

        garbageLocations.filter(function (item) {

            if (item.isOffice == true) {
                officeLocations.push(item);
            } else if (item.isOffice == false) {
                containerLocations.push(item);
            }
        }).map(function ({ id, name, city }) {
            return null;
        });
        return { offices: officeLocations, containers: containerLocations };
    }

    //Actions

    onPressFilterButton = () => {
        var isVisibleFilter = !this.state.visibleFilter;
        this.setState({ visibleFilter: isVisibleFilter });
    };

    onPressMarkerDetailsButton(marker, isPressed) {

        this.updateVisibleMapState(false);

        const { navigation } = this.props

        if (typeof navigation !== "undefined") {
            navigation.push('LocationDetailScreen', { garbage: marker })
        }
    };

    pressMarker(marker) {
        this.setState({ selectedMarker: marker });
    }

    onPressShowedOfficeMarkersButton = () => {

        var isShowedOfficeMarkers = !this.state.showedOfficeMarkers;
        this.showedOfficeMarkers = isShowedOfficeMarkers;
        this.filterGarbageLocations(this.garbageMarkers, true);
        this.setState({ showedOfficeMarkers: this.showedOfficeMarkers });
    }

    onPressShowedContainerMarkersButton = () => {

        var isShowedContainerMarkers = !this.state.showedContainerMarkers;
        this.showedContainerMarkers = isShowedContainerMarkers;
        this.filterGarbageLocations(this.garbageMarkers, true);
        this.setState({ showedContainerMarkers: this.showedContainerMarkers });
    }

    onPressNavigationDirections() {

        if (this.state.selectedMarker) {

            const startPoint = {
                longitude: LocationsService.sharedInstance().getCurrentLongitude(),
                latitude: LocationsService.sharedInstance().getCurrentLatitude()
            }

            const endPoint = {
                longitude: this.state.selectedMarker.longitude,
                latitude: this.state.selectedMarker.latitude
            }

            const transportPlan = 'd';

            OpenMapDirections(startPoint, endPoint, transportPlan).then(res => {
                console.log('Opened Map Directions');
            });
        } else {

            this.showAlert(strings.warningTitle, strings.selectMarkerTitle, strings.okButtonTitle);
        }
    }

    onPressLocationsList() {
        this.updateVisibleMapState(false);
        this.props.navigation.navigate("LocationsListScreen", { activeGarbageMarkers: this.activeGarbageMarkers });
    }

    //Private
    updateMoreActionState() {

        var isMoreAction = !this.state.showedMoreActions;
        this.setState({ showedMoreActions: isMoreAction });
    }

    handleViewRef = ref => this.view = ref;

    handleFilterViewRef = ref => this.view = ref;

    handleMapRegionChange() {
    };

    buttonsAnimation() {
        return this.state.showedMoreActions ? "slideInUp" : "slideInDown";
    }

    filterAnimation() {
        return this.state.visibleFilter ? "slideInUp" : "slideInDown";
    }

    renderCallout(marker) {
        return (
            <MapView.Callout tooltip={true}
                onPress={this.onPressMarkerDetailsButton.bind(this, marker)}>
                <LocationMarkerCallout marker={marker} />
            </MapView.Callout>
        );
    }

    showAlert(title, message, cancelTitle) {

        Alert.alert(
            title,
            message,
            [
                { text: cancelTitle, onPress: () => console.log('Cancel Pressed'), style: 'cancel' }
            ],
            { cancelable: false }
        )
    }

    getLocation() {
        let lng = LocationsService.sharedInstance().getCurrentLongitude();
        let lat = LocationsService.sharedInstance().getCurrentLatitude();
        return { latitude: lat, longitude: lng };
    }

    updateVisibleMapState(isVisibleMap) {

        this.visibleMap = isVisibleMap;
        var showMoreActions = this.state.showedMoreActions;
        if (!isVisibleMap) {
            showMoreActions = false;
        }
        this.setState({ visibleMap: this.visibleMap, 
            showedMoreActions: showMoreActions});
    }

    render() {

        var location = this.getLocation();

        return (
            <View style={styles.container}>
                <NavigationView
                    leftButton={images.menuIcon.uri}
                    rightButton={images.filterIcon.uri}
                    titleLabel={strings.locationsTitle}
                    onPressLeftButton={() => {
                        if (!this.props.navigation.state.isDrawerOpen) {
                            this.props.navigation.dispatch(DrawerActions.openDrawer());
                        } else {
                            this.props.navigation.dispatch(DrawerActions.closeDrawer());
                        }
                    }}
                    onPressRightButton={this.onPressFilterButton}
                />
                <View style={styles.mapsContainer}>
                    <MapView
                        style={styles.map}
                        // provider={PROVIDER_GOOGLE}
                        provider={Platform.OS == 'ios'?PROVIDER_GOOGLE:"google"}
                        onRegionChange={this.handleMapRegionChange.bind(this)}
                        showsUserLocation={this.state.visibleMap}
                        customMapStyle={mapStyle}
                        initialRegion={{
                            latitude: this.state.currentLatitude,
                            longitude: this.state.currentLongitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >
                        {this.activeGarbageMarkers.map(marker => (
                            <MapView.Marker
                                coordinate={{
                                    latitude: marker.latitude,
                                    longitude: marker.longitude
                                }}
                                pinColor={marker.isOffice == true ? '#6753D5' :  'blue'}
                                onPress={() => this.pressMarker(marker)}
                            >
                                {this.renderCallout(marker)}
                            </MapView.Marker>
                        ))
                        }
                    </MapView>
                    <View style={{
                        height: this.showedMoreActions ? 150 : 40,
                        alignSelf: 'flex-end',
                        top: screenHeight - (this.state.showedMoreActions ? 150 : 40) - (DeviceHelper.sharedInstance.isiOS ? 20 : 40) - 44 - 20,
                    }}>
                        {renderIf(this.state.showedMoreActions)(
                            <Animatable.View ref={this.handleViewRef}
                                animation={this.buttonsAnimation()}>
                                <TouchableOpacity
                                    style={styles.buttonContainer}
                                    onPress={() => this.onPressLocationsList()}>
                                    <Image
                                        style={styles.buttonImageContainer}
                                        resizeMode="contain"
                                        source={images.detailMapIcon.uri} />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={styles.buttonContainer}
                                    onPress={() => this.onPressNavigationDirections()}>
                                    <Image
                                        style={styles.buttonImageContainer}
                                        resizeMode="contain"
                                        source={images.navigateMapIcon.uri} />
                                </TouchableOpacity>
                            </Animatable.View>
                        )}
                        <Animatable.View ref={this.handleViewRef}
                            animation={this.buttonsAnimation()}>
                            <TouchableOpacity
                                style={styles.optionButtonContainer}
                                onPress={this.updateMoreActionState.bind(this)}>
                                <Image
                                    style={styles.optionButtonImageContainer}
                                    resizeMode="contain"
                                    source={images.moreMapIcon.uri} />
                            </TouchableOpacity>
                        </Animatable.View>
                    </View>
                    {renderIf(this.state.visibleFilter)(
                        <Animatable.View ref={this.handleFilterViewRef}
                            animation={this.filterAnimation()}>
                            <View style={{
                                height: Dimensions.get("window").height / 3,
                                backgroundColor: '#6753D5',
                                flexDirection: "column",
                                marginTop: Dimensions.get("window").height - Dimensions.get("window").height / 3 - (DeviceHelper.sharedInstance.isiOS ? 20 : 40),
                                width: Dimensions.get("window").width
                            }}>
                                <View style={styles.filterTitleContent}>

                                    <Text style={styles.filterText}>
                                        {strings.filterTitle}
                                    </Text>

                                </View>

                                <View style={styles.checkMarkContainer}>

                                    <CheckMark
                                        checked={this.state.showedOfficeMarkers}
                                        label={strings.officesTitle}
                                        onPressCheckedButton={this.onPressShowedOfficeMarkersButton}
                                    />

                                    <View style={styles.separatorContainer}></View>

                                    <CheckMark
                                        checked={this.state.showedContainerMarkers}
                                        label={strings.containersTitle}
                                        onPressCheckedButton={this.onPressShowedContainerMarkersButton}
                                    />

                                </View>
                            </View>
                        </Animatable.View>
                    )}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#6753D5",
    },
    mapsContainer: {
        flex: 2,
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    buttonContainer: {
        backgroundColor: '#6753D5',
        height: 36,
        width: 36,
        borderRadius: 12,
        flexDirection: "row",
        margin: 6,
        marginRight: 15,
        alignSelf: 'center',
    },
    optionButtonContainer: {
        backgroundColor: '#6753D5',
        height: 44,
        width: 44,
        borderRadius: 12,
        flexDirection: "row",
        margin: 6,
        marginRight: 15,
        alignSelf: 'center',
    },
    buttonImageContainer: {
        alignSelf: 'center',
        marginLeft: 8,
    },
    optionButtonImageContainer: {
        alignSelf: 'center',
        marginLeft: 18,
    },
    filterTitleContent: {
        height: 44,
        width: '100%',
        alignSelf: 'center',
    },
    filterText: {
        color: "#fff",
        fontFamily: "helvetica",
        fontSize: 15,
        textAlign: "right",
        flexDirection: "row",
        marginRight: 15,
        alignItems: "center",
        margin: 10,
        marginBottom: 0,
    },
    checkMarkContainer: {
        flex: 2,
        backgroundColor: '#fff',
        flexDirection: "column",
    },
    separatorContainer: {
        height: 1,
        backgroundColor: '#9A9A9A',
        marginLeft: 10,
        marginRight: 10,
    }
})

export default LocationsScreen;
