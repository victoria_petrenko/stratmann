//LocationMarkerCallout

//Modules
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    ImageBackground,
} from 'react-native';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';

export default class LocationMarkerCallout extends React.Component{
    constructor(props){
        super(props);
        this.state= {
        }
    }
  
    componentDidMount(){
  
    }

    shouldComponentUpdate(nextProps, nextState){
        return nextState != nextProps;
     }

    componentWillUnmount() {
        return null;
    }
  
    render(){
        var marker = this.props.marker;
          return (
            <View style={styles.calloutContainer}>
                    <ImageBackground style={{ margin: 10, marginBottom: 0, width: 200, height: 100 }}
                        resizeMode="cover"
                        source={images.calloutBGIcon.uri}>

                        <Text
                            style={styles.calloutTitle}
                            numberOfLines={1}
                        >
                            {marker.officeName}
                        </Text>

                        <Text
                            style={styles.calloutDescription}
                            numberOfLines={1}
                        >
                            {marker.address + ', ' + marker.country}
                        </Text>

                        <Text
                            style={styles.calloutDetails}
                        >
                            {strings.detailsTitle}
                        </Text>

                    </ImageBackground>
                </View>
          )
    }
}

const styles = StyleSheet.create({
    calloutContainer: {
        flex: 1,
        alignItems: 'center',
    },
    calloutTitle: {
        marginTop: 14,
        color: '#423875',
        fontFamily: "helvetica",
        fontSize: 15,
        textAlign: "left",
        marginLeft: 10,
    },
    calloutDescription: {
        marginTop: 3,
        color: '#8A80BB',
        fontFamily: "helvetica",
        fontSize: 13,
        textAlign: "left",
        marginLeft: 10,
    },
    calloutDetails: {
        marginTop: 8,
        color: '#FF4C74',
        fontFamily: "helvetica",
        fontSize: 13,
        textAlign: "left",
        marginLeft: 10,
        textDecorationLine: 'underline'
    },
});