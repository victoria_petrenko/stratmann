//GarbageTimeLineRow 

//Modules
import React from 'react';
import { View, Text, StyleSheet, Dimensions, } from 'react-native';
import ViewOverflow from 'react-native-view-overflow';

//Services
import GarbageService from '../../services/GarbageService'

const GarbageTimeLineRow = (props) => (
    <View style={styles.container}>
        <View style={styles.timeLineContainer}>
            <Text style={styles.timeLineText}>
                {props.item.dateText.replace("(", "\n(")}
            </Text>
        </View>

        <ViewOverflow style={styles.separatorContainer}>
            <View style={styles.circleLightView}>
                <View style={styles.circleDarkView}>
                </View>
            </View>
        </ViewOverflow>

        <View style={styles.textContainer}>
            <Text style={styles.garbageText}>
                {props.item.garbageTypeText}
            </Text>
            <Text style={styles.garbageDescriptionText}>
                {props.item.garbageDescription}
            </Text>
        </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    timeLineContainer: {
        flexDirection: 'column',
        justifyContent: "flex-start",
        flex: 1,
        marginBottom: 10,
    },
    separatorContainer: {
        alignItems: 'center',
        width: 2,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 0,
        backgroundColor: 'rgba(122,95,238, .10)',
    },
    separatorLine: {
        alignItems: 'center',
        width: 2,
        marginBottom: 0,
        backgroundColor: 'rgba(122,95,238, .10)',
        flexDirection: 'column',
    },
    circleDarkView: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#6753D5',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: 4,
    },
    circleLightView: {
        width: 18,
        height: 18,
        borderRadius: 9,
        backgroundColor: 'rgba(103,83,213,0.4)',
        alignItems: 'center',
        flexDirection: 'column',
    },
    textContainer: {
        flexDirection: 'column',
        justifyContent: "flex-start",
        flex: 2,
        marginBottom: 10,
    },
    timeLineText: {
        fontSize: 14,
        color: '#FF4C74',
        fontFamily: 'helvetica',
        textAlign: 'center',
        marginBottom: 10,
    },
    garbageText: {
        fontSize: 17,
        color: '#423875',
        fontFamily: 'helvetica',
        textAlign: 'left',
        marginLeft: 20,
    },
    garbageDescriptionText: {
        fontSize: 13,
        color: '#8A80BB',
        fontFamily: 'helvetica',
        textAlign: 'left',
        marginLeft: 20,
        marginTop: 4,
        marginBottom: 10,
    },
});

export default GarbageTimeLineRow;