//GarbageTimeLineScreen

//Modules
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList
} from 'react-native';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';

//Services
import GarbageService from "../../services/GarbageService";

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from "../../Navigation/NavigationView";
import GarbageTimeLineRow from './GarbageTimeLineRow';

export class GarbageTimeLineScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            garbageDates: GarbageService.sharedInstance().state.garbageDates,
            calendarInfoDates: GarbageService.sharedInstance().state.garbageInfoData,
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationView
                    leftButton={images.menuBackIcon.uri}
                    titleLabel={strings.timelineTitle}
                    onPressLeftButton={() => {
                        this.props.navigation.navigate("GarbageRemovalScreen");
                    }}
                />
                <FlatList style={styles.garbageFlatList}
                    data={this.state.calendarInfoDates}
                    renderItem={(data) => <GarbageTimeLineRow {...data} />}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#6753D5",
    },
    garbageFlatList: {
        flexDirection: "column",
        backgroundColor: '#fff',
        paddingTop: 10,
    }
})

export default GarbageTimeLineScreen;