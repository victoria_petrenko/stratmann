//MenuScreen
import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper'

export class MenuScreen extends Component {

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      <View>
        <ScrollView style = {styles.scrollContainer}>
        <View style={styles.logoContainer}>
          <Image source={images.menuLogoIcon.uri}
          onPress={this.navigateToScreen('Page1')} />
          </View>
          <TouchableOpacity style={styles.buttonContainer}>
            <View style={styles.timeContainer}>
              <View style={styles.timeImageContainer}>
                <Image source={images.timeIcon.uri} />
                <Text style={styles.buttonText}>{strings.homeTitle}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

MenuScreen.propTypes = {
  navigation: PropTypes.object
};

const styles = StyleSheet.create({
  scrollContainer: {
    width: Dimensions.get('window').width, 
    height: Dimensions.get('window').height - 64 > 600 ? Dimensions.get('window').height - 64 : 600, 
    position: 'absolute',
  },
  logoContainer: {
    position: 'absolute',
    alignItems: 'center',
    top: 24,
    flexDirection: 'column',
    height: 47,
    width: 84, 
    alignSelf: "stretch",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    top: 56,
    height: 50,
  },
  buttonText: {
    color: '#423875',
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: 'left',
    flexDirection: 'row',
    marginLeft: 14,
  },
})

export default MenuScreen;