//ContainerServicesRow.js

//Modules
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
} from 'react-native';
import { ListItem, } from 'react-native-elements';

const ContainerServicesRow = (props) => (
    <ListItem containerStyle={styles.container}
        title={
            <View style={styles.containerView}>
                <Text style={styles.titleText}>{`${props.item.title}`}</Text>
            </View>
        }
        textInput={props.index == 0 ? true : false}
        textInputContainerStyle={{height: 40,}}
        textInputStyle={{fontSize: 15,}}
        textInputPlaceholder={"1"}
        textInputMaxLength={2}
        textInputKeyboardType='number-pad'
        textInputOnChangeText={text => props.onChangeItemText(text)}
        rightTitle={
            <Text style={styles.badgeText}>{`${props.item.value}`}</Text>
        }
        onPress={() => props.onPressItem(props.item)}
    />
);

const styles = StyleSheet.create({
    container: {
        marginLeft: 17,
        marginRight: 17,
    },
    containerView: {
        flexDirection: 'row',
        paddingTop: 5,
        paddingLeft: -15,
    },
    titleText: {
        color: '#423875',
        paddingLeft: -15,
        fontFamily: "helvetica",
        fontSize: 17,
        textAlign: "left",
    },
    badgeView: {
        backgroundColor: 'transparent',
        paddingTop: 5,
    },
    badgeText: {
        color: '#423875',
        fontFamily: "helvetica",
        fontSize: 13,
        textAlign: "right",
    },
});

export default ContainerServicesRow;