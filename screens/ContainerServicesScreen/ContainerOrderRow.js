//ContainerOrderRow.js

//Modules
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image,
    TouchableOpacity,
} from 'react-native';

//Helpers
import { images } from '../../Helpers/Image+Helper';

const screenWidth = Dimensions.get('window').width;

const ContainerServicesRow = (props) => (
    <View style={styles.container}>
        <Text style={styles.titleText}>{`${props.item.pieceValue}x${props.item.pieceTitle}`}</Text>
        <Text style={styles.titleText}>{`${props.item.wasteTitle}: ${props.item.wasteValue}`}</Text>
        <Text style={styles.titleText}>{`${props.item.actionTitle}: ${props.item.actionValue}`}</Text>
        <TouchableOpacity
            style={styles.closeButton}
            onPress={() => props.onPressItem(props.index, props.item)}
            >
            <Image style={styles.closeImageContainer}
                resizeMode="contain" source={images.closeIcon.uri} />
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        marginTop: 10,
        marginBottom: 10,
    },
    titleText: {
        color: '#423875',
        marginLeft: 16,
        fontFamily: "helvetica",
        fontSize: 15,
        textAlign: "left",
        marginBottom: 6,
    },
    closeButton: {
        flexDirection: "row",
        width: 50,
        height: 50,
        position: 'absolute',
        marginLeft: screenWidth - 65,
        marginTop: 5,
    },
    closeImageContainer: {
        marginLeft: 25,
        width: 17,
        height: 17,
    },
});

export default ContainerServicesRow;