//ContainerServicesScreen
import React, { Component } from 'react';

//Modules
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  FlatList,
  Alert,
  TouchableOpacity,
  TextInput,
  ImageBackground,
} from 'react-native';
import { DrawerActions } from "react-navigation";
import SegmentedControlTab from 'react-native-segmented-control-tab'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Orientation from 'react-native-orientation';
import InputScrollView from 'react-native-input-scroll-view';
import ImagePicker from 'react-native-image-picker';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';
import renderIf from '../../Helpers/renderIf';
import DeviceHelper from '../../Helpers/Device+Helper';

//Components
import { DrawerLabel } from '../../Navigation/DrawerLabel';
import { NavigationView } from "../../Navigation/NavigationView";
import ContainerServicesRow from './ContainerServicesRow';
import ContainerOrderRow from './ContainerOrderRow';
import GradientButton from "../../components/GradientButton";
import GreyTextInput from '../../components/GreyTextInput';

//Services
import GarbageService from '../../services/GarbageService';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const CARUSEL_WIDTH = 3 * screenWidth;
const CARUSEL_ITEM_WIDTH = screenWidth;
const CONTAINER_ORDER_ROW_HEIGHT = 95;

const cameraOptions = {
  title: strings.choosePhoto,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const AddPhotoIndexEnum = {
  AddPhotoFirstIndex: 1,
  AddPhotoSecondIndex: 2,
  AddPhotoThirdIndex: 3,
  count: 3,
};

export class ContainerServicesScreen extends Component {

  static navigationOptions = {
    drawerLabel: (
      <DrawerLabel
        label={strings.containerServiceTitle}
        icon={images.menuContainerIcon.uri}
      />
    ),
  }

  //Props
  constructor(props) {
    super(props);

    this.willFocusSubscription = null;
    this.selectedIndex = 0;
    this.containersData = this.getContainersData();
    this.settingsContainerData = this.getSettingsContainerInfo();
    this.orderListData = [];
    this.selectedWaste = null;
    this.noteText = null;
    this.firstPhotoUri = null;
    this.secondPhotoUri = null;
    this.thirdPhotoUri = null;

    this.state = {
      selectedIndex: this.selectedIndex,
      containersData: this.containersData,
      activeSlide: 0,
      settingsContainerData: this.settingsContainerData,
      orderListData: this.orderListData,
      noteText: this.noteText,
      firstPhotoUri: this.firstPhotoUri,
      secondPhotoUri: this.secondPhotoUri,
      thirdPhotoUri: this.thirdPhotoUri,
    };
  }

  //Life-Cycle
  componentWillMount() {

    Orientation.lockToPortrait();
    Orientation.addOrientationListener(() => Orientation.lockToPortrait());

    //Add Will Focus Listener
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      payload => {
        if (payload.state.params) {
          if (typeof payload.state.params.selectedWaste !== "undefined") {
            this.selectedWaste = payload.state.params.selectedWaste;
            this.updateComponent();
          }
        }
      }
    );
  }

  updateComponent = () => {
    this.settingsContainerData = this.getSettingsContainerInfo();
    this.setState({ settingsContainerData: this.settingsContainerData });
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }


  //Private
  getContainersData() {

    var containers = [];

    var container1 = { title: strings.smallContainerTitle, image: images.smallContainerImage.uri };
    containers.push(container1);

    var container2 = { title: strings.smallPaperContainerTitle, image: images.smallPaperContainerImage.uri };
    containers.push(container2);

    var container3 = { title: strings.middleContainerTitle, image: images.middleContainerImage.uri };
    containers.push(container3);

    var container4 = { title: strings.middleMetalContainerTitle, image: images.middleMetallContainerImage.uri };
    containers.push(container4);

    var container5 = { title: strings.bigContainerTitle, image: images.bigContainerImage.uri };
    containers.push(container5);

    return containers;
  }

  getSettingsContainerInfo() {

    var settingsData = [];

    if (this.selectedWaste) {

      settingsData = this.settingsContainerData;
      if (settingsData.length > 2) {
        var waste = settingsData[1];
        waste.value = this.selectedWaste.descriptionTitle;
        settingsData[1] = waste;
      }
    } else {
      var numberOfPieces = { title: strings.numberOfPiecesTitle, value: '1', index: 0 };
      settingsData.push(numberOfPieces);

      var waste = { title: strings.wasteRowTitle, value: GarbageService.sharedInstance().state.garbageTypeDetailsData[0].garbageDetailDescription[0], index: 1 };
      settingsData.push(waste);

      var action = { title: strings.actionTitle, value: strings.setUpTitle, index: 2 };
      settingsData.push(action);
    }
    return settingsData;
  }

  isPhotoSegment() {
    return this.selectedIndex == 1;
  }

  addOrder() {

    var orderListArray = this.orderListData;

    var order = {
      pieceValue: '',
      pieceTitle: '',
      wasteValue: '',
      wasteTitle: '',
      actionValue: '',
      actionTitle: ''
    };

    var settingsList = this.settingsContainerData;
    for (index = 0; index < settingsList.length; index++) {

      var settingsData = settingsList[index];
      let settingsValue = settingsData.value;
      let settingsTitle = settingsData.title;

      if (index == 0) {
        order.pieceValue = settingsValue;
        order.pieceTitle = this.containersData[this.state.activeSlide].title;
      } else if (index == 1) {
        order.wasteValue = settingsValue;
        order.wasteTitle = settingsTitle;
      } else if (index == 2) {
        order.actionValue = settingsValue;
        order.actionTitle = settingsTitle;
      }
    }
    orderListArray.push(order);
    this.orderListData = orderListArray;
    this.setState({ orderListData: this.orderListData });
  }

  showAlert(title, message, cancelTitle) {

    Alert.alert(
      title,
      message,
      [
        { text: cancelTitle, onPress: () => this.closeScreen(), style: 'cancel' }
      ],
      { cancelable: false }
    )
  }

  closeScreen() {

    const { navigation } = this.props

    if (typeof navigation !== "undefined") {
      navigation.navigate("HomeScreen");
    }
  }

  updateNumberOfPiecesValue = (textValue) => {

    var settingsData = this.settingsContainerData;
    if (settingsData.length > 1) {
      var pieceInfo = settingsData[0];
      pieceInfo.value = textValue.length > 0 ? textValue : "1";
      settingsData[0] = pieceInfo;
    }
    this.settingsContainerData = settingsData;
    this.setState({ settingsContainerData: this.settingsContainerData });
  }

  //Actions
  handleIndexChange = (index) => {
    this.selectedIndex = index;
    this.setState({
      selectedIndex: index,
    });
  }

  showSettingsItem = (item) => {
    const { navigation } = this.props

    if (item.index == 1) {
      if (typeof navigation !== "undefined") {
        navigation.push("WasteScreen", { isContainerServiceWaste: true });
      }
    }
  }

  onPressAddButton() {
    this.addOrder();
  }

  onPressSendButton() {
    this.showAlert("", strings.acceptedApplication, strings.okButtonTitle);
    this.setState({
      firstPhotoUri: null,
      secondPhotoUri: null,
      thirdPhotoUri: null,
      noteText: null,
    })
  }

  onPressCheckoutOrderButton() {
    this.showAlert("", strings.orderIssuedTitle, strings.okButtonTitle);
  }

  onPressCloseOrderItem = (index, item) => {

    if (index < this.orderListData.length) {

      //Delete Order
      this.orderListData.splice(index, 1);
      this.setState({
        orderListData: this.orderListData,
      });
    }
  }

  onPressAddPhotoButton = (addButtonIndex) => {

    ImagePicker.showImagePicker(cameraOptions, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        switch (addButtonIndex) {
          case AddPhotoIndexEnum.AddPhotoFirstIndex: {
            this.firstPhotoUri  =  source
            this.setState({ firstPhotoUri: source })
            break;
          }
          case AddPhotoIndexEnum.AddPhotoSecondIndex: {
            this.secondPhotoUri =  source
            this.setState({ secondPhotoUri: source })
            break;
          }
          case AddPhotoIndexEnum.AddPhotoThirdIndex: {
            this.thirdPhotoUri = source
            this.setState({ thirdPhotoUri: source })
            break;
          }
        }
      }
    });
  };

  //Rendering
  renderCaruselItem({ item, index }) {
    return (
      <View style={styles.caruselItemContainer}>
        <Image
          source={item.image}
          style={styles.imageСaruselContainer}
        />
        <Text style={styles.caruselTitle} numberOfLines={2}>
          {item.title}
        </Text>
      </View>
    );
  }

  get pagination() {
    const { activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={5}
        activeDotIndex={activeSlide}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 8,
          backgroundColor: '#6753D5'
        }}
        inactiveDotStyle={{
          backgroundColor: '#B5ADE3'
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  render() {

    var isPfotoSegment = this.isPhotoSegment();
    return (
      <View style={styles.container}>

        <NavigationView
          leftButton={images.menuIcon.uri}
          titleLabel={strings.containerServiceTitle}
          onPressLeftButton={() => {
            if (!this.props.navigation.state.isDrawerOpen) {
              this.props.navigation.dispatch(DrawerActions.openDrawer());
            } else {
              this.props.navigation.dispatch(DrawerActions.closeDrawer());
            }
          }}
        />

        <View style={styles.segmentContainer}>
          <SegmentedControlTab
            values={[strings.inquiryTitle, strings.photoInquiryTitle]}
            selectedIndex={this.state.selectedIndex}
            onTabPress={this.handleIndexChange}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            tabTextStyle={styles.tabTextStyle}
            activeTabTextStyle={styles.activeTabTextStyle}
          >
          </SegmentedControlTab>
        </View>

        <View style={styles.contentCotainer}>
          {renderIf(!isPfotoSegment)(
            <InputScrollView contentContainerStyle={{ flex: 0 }}
              keyboardShouldPersistTaps
            >
              <View style={styles.caruselContainer}>
                <View style={{
                  height: 210,
                  flexDirection: "column",
                }}>
                  <Carousel
                    ref={(c) => { this._carousel = c; }}
                    data={this.state.containersData}
                    renderItem={this.renderCaruselItem}
                    layout={'default'}
                    sliderWidth={CARUSEL_ITEM_WIDTH}
                    itemWidth={CARUSEL_ITEM_WIDTH}
                    onSnapToItem={(index) => this.setState({ activeSlide: index })}>
                  </Carousel>
                </View>
                {this.pagination}
                <View style={styles.flatListContainer}>
                  <FlatList style={styles.settingsFlatList}
                    scrollEnabled={false}
                    data={this.state.settingsContainerData}
                    extraData={this.state}
                    renderItem={(data) => <ContainerServicesRow {...data}
                      onPressItem={(item) => this.showSettingsItem(item)}
                      onChangeItemText={(text) => this.updateNumberOfPiecesValue(text)} />}
                  />
                </View>
                <GradientButton
                  title={strings.addButtonTitle}
                  onPress={this.onPressAddButton.bind(this)}>
                </GradientButton>

                {renderIf(this.state.orderListData.length > 0)(
                  <View style={styles.orderView}>
                    <View style={styles.headerCheckoutContainer}>
                      <Text style={styles.headerText}>
                        {strings.orderListTitle}
                      </Text>
                    </View>

                    <FlatList style={{
                      flexDirection: "column",
                      height: CONTAINER_ORDER_ROW_HEIGHT * this.state.orderListData.length,
                    }}
                      data={this.state.orderListData}
                      scrollEnabled={false}
                      renderItem={({ item, index }) => {
                        return (
                          <ContainerOrderRow
                            item={item}
                            index={index}
                            onPressItem={this.onPressCloseOrderItem}
                          />
                        );
                      }}
                    />

                    <GradientButton
                      title={strings.checkoutOrderButtonTitle}
                      onPress={this.onPressCheckoutOrderButton.bind(this)}>
                    </GradientButton>
                  </View>)}
              </View>
            </InputScrollView>
          )}
          {renderIf(isPfotoSegment)(
            <View >
              <InputScrollView>
                <Text style={styles.sendThreePhotosText}>
                  {strings.sendThreePhotosDesciption}
                </Text>

                <View style={styles.addPhotoViewContent}>

                  <TouchableOpacity style={styles.addPhotoButton}
                    onPress={() => this.onPressAddPhotoButton(AddPhotoIndexEnum.AddPhotoFirstIndex)}>
                    <Image style={this.state.firstPhotoUri ? styles.selectedPhotoImage : styles.addPhotoImage}
                      resizeMode={"cover"}
                      borderRadius={3}
                      source={this.state.firstPhotoUri ? this.state.firstPhotoUri : images.addIcon.uri} />
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.addPhotoButton}
                    onPress={() => this.onPressAddPhotoButton(AddPhotoIndexEnum.AddPhotoSecondIndex)}>
                    <Image style={this.state.secondPhotoUri ? styles.selectedPhotoImage : styles.addPhotoImage}
                      resizeMode={"cover"}
                      borderRadius={3}
                      source={this.state.secondPhotoUri ? this.state.secondPhotoUri : images.addIcon.uri} />
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.addPhotoButton}
                    onPress={() => this.onPressAddPhotoButton(AddPhotoIndexEnum.AddPhotoThirdIndex)}>
                    <Image style={this.state.thirdPhotoUri ? styles.selectedPhotoImage : styles.addPhotoImage}
                      resizeMode={"cover"}
                      borderRadius={3}
                      source={this.state.thirdPhotoUri ? this.state.thirdPhotoUri : images.addIcon.uri} />
                  </TouchableOpacity>
                </View>

                <Text style={styles.notationText}>
                  {strings.noteTitle}
                </Text>

                <GreyTextInput
                  useBorder={true}
                  placeholder={strings.addYourComments}
                  onChangeText={(text) => this.setState({ noteText: text })}>
                </GreyTextInput>

                <GradientButton
                  title={strings.sendButtonTitle}
                  onPress={this.onPressSendButton.bind(this)}>
                </GradientButton>

              </InputScrollView>
            </View>
          )}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#6753D5",
  },
  segmentContainer: {
    margin: 10,
    marginLeft: 17,
    marginRight: 17,
  },
  tabStyle: {
    backgroundColor: "#6753D5",
    borderColor: "#fff"
  },
  activeTabStyle: {
    backgroundColor: "#fff",
    borderColor: "#fff"
  },
  tabTextStyle: {
    color: "#fff",
    fontSize: 13,
  },
  activeTabTextStyle: {
    color: "#6753D5",
    fontSize: 13,
  },
  contentCotainer: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
  },
  caruselContainer: {
    flex: 1,
    flexDirection: "column",
  },
  caruselItemContainer: {
    marginTop: 40,
    flexDirection: "column",
    height: 150,
    justifyContent: 'center',
  },
  imageСaruselContainer: {
    flexDirection: "column",
    height: 138,
    width: screenWidth - 100,
    marginLeft: 50,
    marginRight: 50,
    justifyContent: 'center',
    resizeMode: 'contain'
  },
  caruselTitle: {
    marginTop: 22,
    color: "#423875",
    fontSize: 15,
    alignItems: 'center',
    textAlign: "center",
    fontFamily: "helvetica",
  },
  paginationContainer: {
    flexDirection: "column",
    justifyContent: 'center',
  },
  orderView: {
    flexDirection: "column",
    flex: 2,
  },
  flatListContainer: {
    flexDirection: "column",
    height: 170,
  },
  settingsFlatList: {
    flexDirection: "column",
  },
  headerCheckoutContainer: {
    backgroundColor: '#F4F4F4',
    flexDirection: "column",
    height: 44,
    justifyContent: 'center',
  },
  headerText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 17,
    textAlign: "left",
    marginLeft: 16,
  },
  sendThreePhotosText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: "left",
    marginLeft: 16,
    marginTop: 20,
  },
  addPhotoViewContent: {
    height: 70,
    flexDirection: "row",
    marginLeft: 16,
    marginRight: 16,
    justifyContent: 'center',
  },
  addPhotoButton: {
    flexDirection: "row",
    flexWrap: "wrap",
    borderRadius: 5,
    backgroundColor: "#F6F4FF",
    width: '28%',
    height: 70,
    justifyContent: "space-between",
    marginTop: 20,
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  photoButton: {
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: "#F6F4FF",
    width: '28%',
    marginTop: 20,
    height: 70,
    justifyContent: "space-between",
    justifyContent: 'center',
    overflow: 'hidden',
  },
  addPhotoImage: {
    top: 4,
    alignSelf: 'center',
    alignItems: 'center',
  },
  selectedPhotoImage: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    resizeMode: 'cover'
  },
  notationText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: "left",
    marginLeft: 16,
    marginTop: 60,
  },
  textInputNotes: {
    fontFamily: "helvetica",
    fontSize: 15,
    borderColor: '#979797',
    borderWidth: 1,
    backgroundColor: '#EFEFF4',
    color: 'black',
    marginLeft: 16,
    marginRight: 16,
    marginTop: 10,
    height: 130,
    paddingLeft: 8,
    paddingTop: 14,
    marginBottom: 10,
    textAlignVertical: "top"
  },
})

export default ContainerServicesScreen;