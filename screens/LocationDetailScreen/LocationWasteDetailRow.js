//LocationWasteDetailRow.js

import React from 'react';

//Modules
import { View, Text, StyleSheet, Dimensions, } from 'react-native';

//Services
import GarbageService from '../../services/GarbageService';

const LocationWasteDetailRow = (props) => (

    <View style={styles.container}>

        <Text style={styles.text}>
        {GarbageService.sharedInstance().garbageDescription(props.item)}
        </Text>

        <View style={styles.separator}>
        </View>

    </View>
);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        marginLeft: 16,
        marginRight: 16,
        height: 44,
        justifyContent: 'center',
    },
    separator: {
        height: 1,
        backgroundColor: '#D8D8D8',
    },
    text: {
        fontSize: 15,
        color: '#423875',
        fontFamily: 'helvetica',
        textAlign: 'left',
        marginLeft: 15,
        marginBottom: 12,
    },
});

export default LocationWasteDetailRow;