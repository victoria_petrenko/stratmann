//LocationDetailScreen.js

//Modules
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ListView,
  FlatList
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { EventRegister } from 'react-native-event-listeners';
import Orientation from 'react-native-orientation';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';
import DeviceHelper from '../../Helpers/Device+Helper';
import renderIf from '../../Helpers/renderIf';

//Components
import { NavigationView } from "../../Navigation/NavigationView";
import GarbageWorkDayRow from "./GarbageWorkDayRow";
import LocationWasteDetailRow from './LocationWasteDetailRow';

const mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]

  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
];

const dayes = {
  "en": ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
  "de": ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
  "ru": ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
};

var screenHeight = Dimensions.get('window').height;

export class LocationDetailScreen extends Component {

  //Props
  constructor(props) {
    super(props);

    this.garbage = this.props.navigation.state.params.garbage;
    this.workDayesData = this.getWorkDayesData();

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      garbage: this.garbage,
      workDayesData: this.workDayesData,
      dataSource: ds.cloneWithRows(this.workDayesData),
      isPortrait: Orientation.orientation == 'PORTRAIT' ? true : false,
    }
  }

  //Life-Cycle
  componentWillMount() {

    // Event Listener for orientation changes
    Dimensions.addEventListener('change', () => {
      screenHeight = Dimensions.get('window').height;
      this.setState({
        isPortrait: Orientation.orientation == 'PORTRAIT' ? true : false,
      });
    });
  }

  componentWillUnmount() {
    this.garbage = null;
  }

  //Private
  getWorkDayesData() {

    var currentLocale = DeviceHelper.sharedInstance().getDeviceLocale();
    var workDayes = dayes[currentLocale];

    let startWorkHours = this.garbage.startWorkHours;
    let endWorkHours = this.garbage.endWorkHours;

    var obj = [];
    for (let i = 0; i < workDayes.length; i++) {
      obj.push({ day: workDayes[i], workHours: `${startWorkHours}-${endWorkHours}` });
    }
    return obj;
  }

  render() {
    return (
      <View style={styles.container}>

        <NavigationView
          leftButton={images.menuBackIcon.uri}
          titleLabel={strings.locationDetailTitle}
          onPressLeftButton={() => {
            this.props.navigation.goBack();
          }}
        />

        <View style={{
          height: screenHeight / 3,
          alignItems: 'center',
        }}>
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            customMapStyle={mapStyle}
            initialRegion={{
              latitude: this.state.garbage.latitude,
              longitude: this.state.garbage.longitude,
              latitudeDelta: 30,
              longitudeDelta: 30,
            }}
            pitchEnabled={false}
            rotateEnabled={false}
            scrollEnabled={false}
            zoomEnabled={false}
          >
            <MapView.Marker
              coordinate={{
                latitude: this.state.garbage.latitude,
                longitude: this.state.garbage.longitude
              }}
              pinColor={this.state.garbage.isOffice == true ? '#6753D5' : 'rgba(152,137,239, 0.9)'}
            >
            </MapView.Marker>
          </MapView>
        </View>

        <View style={{
          backgroundColor: '#fff',
          flexDirection: "column",
          flex: this.state.garbage.isOffice ? 1 : null,
        }}>

          <View style={styles.garbageInfoContainer}>

            <Text style={styles.garbageNameText}>
              {this.state.garbage.officeName}
            </Text>

            <Text style={styles.addressText}>
              {`${this.state.garbage.address}`}
            </Text>

            <Text style={styles.cityText}>
              {`${this.state.garbage.cityIndex} ${this.state.garbage.country}`}
            </Text>

          </View>

          <ListView
            horizontal={true}
            style={{ height: 65, margin: 15 }}
            dataSource={this.state.dataSource}
            renderRow={(data) => <GarbageWorkDayRow {...data} />}
          />
          {renderIf(!this.state.garbage.isOffice)(

            <View style={{ backgroundColor: '#fff', height: 300,}}>

              <View style={styles.headerViewContainer}>
                <Text style={styles.headerText}>
                  {strings.acceptanceOfWasteTitle}
                </Text>
              </View>

              <FlatList style={styles.flatListContainer}
                data={this.state.garbage.garbageType}
                renderItem={(item) => <LocationWasteDetailRow {...item} />}
              />
            </View>
          )}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "#6753D5",
    height: 800,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  garbageInfoContainer: {
    flexDirection: "column",
    marginLeft: 16,
    marginTop: 16,
  },
  garbageNameText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 15,
    textAlign: "left",
  },
  addressText: {
    color: "#9B9B9B",
    fontFamily: "helvetica",
    fontSize: 13,
    textAlign: "left",
    marginTop: 20,
  },
  cityText: {
    color: "#9B9B9B",
    fontFamily: "helvetica",
    fontSize: 13,
    textAlign: "left",
    marginTop: 3,
  },
  headerViewContainer: {
    backgroundColor: '#F4F4F4',
    flexDirection: "column",
    height: 44,
    justifyContent: 'center',
  },
  headerText: {
    color: "#423875",
    fontFamily: "helvetica",
    fontSize: 17,
    textAlign: "left",
    marginLeft: 24,
  },
  flatListContainer: {
    backgroundColor: '#fff',
    flex: 2,
  }
})

export default LocationDetailScreen;