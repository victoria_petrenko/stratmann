//GarbageWorkDayRow.js

//Modules
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
} from 'react-native';

export default class GarbageWorkDayRow extends Component {

    //Props
    constructor(props) {
        super(props);

        this.state = {
            dimensionWidth: Dimensions.get('window').width,
        }

        // Event Listener for orientation changes
        Dimensions.addEventListener('change', () => {

            var newWidth = Dimensions.get('window').width;
            if (newWidth != this.state.dimensionWidth) {
                this.setState({
                    dimensionWidth: Dimensions.get('window').width,
                });
            }
        });
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                width: (this.state.dimensionWidth - 30) / 7,}}>

                <View style={styles.separatorView}>
                </View>

                <Text style={styles.textContainer}>
                    {this.props.day}
                </Text>

                <View style={styles.separatorView}>
                </View>

                <Text style={styles.textContainer}>
                    {this.props.workHours}
                </Text>

            </View>)
    }
}

var styles = StyleSheet.create({
    separatorView: {
        flexDirection: 'column',
        marginTop: 5,
        backgroundColor: '#D8D8D8',
        height: 1,
    },
    textContainer: {
        color: '#423875',
        fontFamily: "helvetica",
        fontSize: 13,
        textAlign: "center",
        marginTop: 10,
    },
});