//LocationsListRow.js

//Modules
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import ViewOverflow from 'react-native-view-overflow';
import { ListItem, } from 'react-native-elements';

//Services
import GarbageService from '../../services/GarbageService'

const LocationsListRow = (props) => (
    <ListItem containerStyle={styles.container}
        title={
            <View style={styles.containerView}>
                <Text style={styles.addressText}>{`${props.item.address}`}</Text>
            </View>
        }
        subtitle={
            <View style={styles.containerView}>
                <Text style={styles.countryText}>{`${props.item.cityIndex} ${props.item.country}`}</Text>
            </View>}
        onPress={() => props.onPressItem(props.item)}
    />
);

const styles = StyleSheet.create({
    container: {
        marginLeft: 17,
        marginRight: 17,
    },
    containerView: {
        flexDirection: 'row',
        paddingTop: 5,
        paddingLeft: -15,
    },
    addressText: {
        color: '#423875',
        paddingLeft: -15,
        fontFamily: "helvetica",
        fontSize: 17,
        textAlign: "left",
    },
    countryText: {
        color: '#8A80BB',
        paddingLeft: -15,
        fontFamily: "helvetica",
        fontSize: 13,
        textAlign: "left",
    }
});

export default LocationsListRow;