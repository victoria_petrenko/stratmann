//LocationsListScreen.js

//Modules
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Platform
} from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import { SearchBar } from 'react-native-elements';
import ViewOverflow from 'react-native-view-overflow';
import { createFilter } from 'react-native-search-filter';

//Helpers
import { images } from '../../Helpers/Image+Helper';
import { strings } from '../../Helpers/Localization+Helper';

//Services
import LocationsService from '../../services/LocationsService';
import DBService from '../../services/DBService';

//Components
import { NavigationView } from "../../Navigation/NavigationView";
import LocationsListRow from "./LocationsListRow";


const KEYS_TO_FILTERS = ['address'];

export class LocationsListScreen extends Component {

  constructor(props) {
    super(props);

    this.searchBar = null;
    this.isSearch = false;
    this.searchQuery = "";
    this.garbageMarkers = this.props.navigation.state.params.activeGarbageMarkers;

    this.state = {
      garbageMarkers: this.garbageMarkers,
      searchBar: this.searchBar,
      isSearch: this.isSearch,
    };
  }

  //Life-Cycle
  componentWillMount() {

    //Add listener for updating garbageMarkers
    EventRegister.addEventListener('changedDBLocationList', (keys) => {
      this.setState({
        garbageMarkers: DBService.sharedInstance().geLocationsData(),
      });
    });
  }

  setSearchBar(ref) {

    if (ref != null && this.searchBar == null) {

      this.searchBar = ref;
      this.setState({
        searchBar: this.searchBar,
      });
    }
  }

  isActiveSearch = (isActive, canceled) => {

    if (this.isSearch == isActive) {
      return;
    }

    this.isSearch = isActive;
    this.setState({
      isSearch: this.isSearch,
    });

    if (!this.isSearch && !isActive && !canceled) {
      // this.searchBar.onCancel();
    }
  }

  //Actions
  handleSearchQueryChange = (query, canceled) => {

    var isCanceled = canceled ? canceled : false;

    this.searchQuery = query || "";

    this.setState({
      searchQuery: this.searchQuery,
    });
    this.isActiveSearch(query.length > 0, isCanceled);
  }

  handleSearchCancel = () => {
    this.handleSearchQueryChange("", true);
  }

  handleSearchClear = () => {
    this.handleQueryChange("", true);
  }

  showLocationDetailScreen = (garbage) => {
    const { navigation } = this.props

    if (typeof navigation !== "undefined") {
      navigation.push('LocationDetailScreen', { garbage: garbage })
    }
  }

  render() {

    const filteredLocations = this.isSearch ? this.state.garbageMarkers.filter(createFilter(this.state.searchQuery, KEYS_TO_FILTERS)) : this.state.garbageMarkers;

    return (
      <View style={styles.container}>

        <NavigationView
          leftButton={images.menuBackIcon.uri}
          titleLabel={strings.locationsListNavigationTitle}
          onPressLeftButton={() => {
            this.props.navigation.goBack();
          }}
        />

        <SearchBar
          containerStyle={styles.searchBarViewContainer}
          inputStyle={styles.searchBarTextInput}
          ref={search => this.setSearchBar(search)}
          autoFocus={this.isSearch}
          platform="ios"
          searchIcon
          clearIcon={this.isSearch}
          placeholderTextColor='#8D7EE0'
          placeholder={strings.searchTitle}
          onChangeText={this.handleSearchQueryChange}
          onCancel={this.handleSearchCancel}
          onClear={this.handleSearchClear}
          value={this.searchQuery}>
        </SearchBar>


        <View style={styles.searchSeparatorView}>
          <ViewOverflow style={styles.searchSeparatorContainer}>
          </ViewOverflow>
        </View>

        <FlatList style={styles.garbageFlatList}
          data={filteredLocations}
          renderItem={(data) => <LocationsListRow {...data}
            onPressItem={(item) => this.showLocationDetailScreen(item)} />}
        />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#6753D5",
  },
  searchBarViewContainer: {
    backgroundColor: "#6753D5",
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
    height: 44,
  },
  searchBarTextInput: {
    borderTopColor: 'transparent',
    backgroundColor: '#6753D5',
    borderBottomColor: '#8D7EE0',
  },
  searchSeparatorView: {
    backgroundColor: '#6753D5',
    height: 8,
    width: '100%',
    flexDirection: "column",
    alignSelf: 'center',
  },
  searchSeparatorContainer: {
    backgroundColor: '#8D7EE0',
    height: 1,
    width: '92%',
    flexDirection: "column",
    alignSelf: 'center',
  },
  garbageFlatList: {
    flexDirection: "column",
    backgroundColor: '#fff',
    marginTop: 10,
  }
})

export default LocationsListScreen;